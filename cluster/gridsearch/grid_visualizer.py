from dash import Dash, dcc, html, Input, Output
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
import itertools
from collections import defaultdict
import numpy as np
from pathlib import Path
from copy import deepcopy
from time import perf_counter

from dataclasses import dataclass
@dataclass
class Mesh:
    x:list[float]
    y:list[float]
    z:list[float]
    i:list[int]
    j:list[int]
    k:list[int]
    def __repr__(self):
        return ' '.join([c+':'+str(getattr(self, c).shape) for c in 'xyzijk'])

def compute_boundary(tetras):
    tris = list(itertools.chain(*([tuple(sorted(p)) for p in itertools.combinations(tet, r=3)] for tet in tetras)))
    outertris = set()
    for tri in tris:
        assert(tri[0]<tri[1] and tri[1]<tri[2])
        if tri in outertris:
            outertris.remove(tri)
        else:
            outertris.add(tri)
    outerpoints_set = set(p for tri in outertris for p in tri)
    outerpoints = list(sorted(outerpoints_set))
    mapping = {all_id:outer_id for outer_id, all_id in enumerate(outerpoints)}
    outertris = np.array([[mapping[a], mapping[b], mapping[c]] for (a,b,c) in outertris], dtype=int)

    return outerpoints, outertris

def get_datagrid(root, nparams=2):
    root = Path(root)
    datagrid = {}
    params = defaultdict(set)
    for subdir in root.iterdir():
        data = np.load(subdir/'cshape_records.npz')
        tokens = subdir.name.split('_')
        # print(str(subdir.name), str(subdir.stem))
        key = tuple()
        for i in range(nparams):
            paramValue = float(tokens[-2*i-1])
            paramName = tokens[-2*i-2]
            params[paramName].add(paramValue)
            key = key + (paramValue,)

        tetras = data['tets']
        datagrid[key] = (data['body_positions'][::10], tetras)
    new_params = {k:sorted(list(v)) for k, v in params.items()}

    return datagrid, new_params

def get_body_boundaries(datagrid):
    meshgrid = {}
    for key, (pos, tetras) in datagrid.items():
        outerpoints, outertris = compute_boundary(tetras)
        pos = pos[:,outerpoints]
        meshes = [Mesh(p[:,0], p[:,1], p[:,2], outertris[:,0], outertris[:,1], outertris[:,2]) for p in pos]
        meshgrid[key] = meshes
    return meshgrid



datagrid, params = get_datagrid('/home/alexandre/workspace/larva-bodies/history/17102024_140128', nparams=3)
meshgrid = get_body_boundaries(datagrid)
PARAMS = {n:list(sorted(s)) for n, s in params.items()}
OPTIONS = list(params.keys())

default_params_checklist = {o:[PARAMS[o][0], PARAMS[o][len(PARAMS[o])//2], PARAMS[o][-1]] for o in OPTIONS}
default_params_radio = {o:PARAMS[o][len(PARAMS[o])//2] for o in OPTIONS}

default_params = {OPTIONS[0]:[PARAMS[OPTIONS[0]][0], PARAMS[OPTIONS[0]][len(PARAMS[OPTIONS[0]])//2], PARAMS[OPTIONS[0]][-1]],
                  OPTIONS[1]:[PARAMS[OPTIONS[1]][0], PARAMS[OPTIONS[1]][len(PARAMS[OPTIONS[1]])//2], PARAMS[OPTIONS[1]][-1]]}
default_params.update({OPTIONS[i]:PARAMS[OPTIONS[i]][len(PARAMS[OPTIONS[i]])//2] for i in range(2, len(OPTIONS))})


def display_meshgrid(meshgrid, params=default_params, row=OPTIONS[0], col=OPTIONS[1], show_axes=False):
    print(200*'-')
    start = perf_counter()
    def get_key(prow, pcol):
        return tuple([prow if o==row else (pcol if o==col else params[o]) for o in OPTIONS])
    
    n_rows = len(params[row])
    n_cols = len(params[col])
    n_plots = n_rows*n_cols
    fig = make_subplots(n_rows, n_cols,
                        specs=[[dict(type='scene') for _ in params[col]] for _ in params[row]],
                        shared_xaxes='all',
                        shared_yaxes='all')
    
    for (i,prow), (j,pcol) in itertools.product(enumerate(params[row]), enumerate(params[col])):
        key = get_key(prow, pcol)
        m = meshgrid[key][0]
        # m = meshgrid[(yms[0], fmax[0])][0]
        fig.add_trace(
            go.Mesh3d(
                x=m.x, y=m.y, z=m.z, 
                i=m.i, j=m.j, k=m.k,
                color='grey'),
            row=i+1,
            col=j+1
            )

    n_frames = len(meshgrid[get_key(params[row][0], params[col][0])])
    step = perf_counter()
    print('step 1 : ', step-start, 's')
    start = step
    frames = [go.Frame(data=[go.Mesh3d(x=meshgrid[get_key(prow, pcol)][t].x, y=meshgrid[get_key(prow, pcol)][t].y, z=meshgrid[get_key(prow, pcol)][t].z, 
                                       i=meshgrid[get_key(prow, pcol)][t].i, j=meshgrid[get_key(prow, pcol)][t].j, k=meshgrid[get_key(prow, pcol)][t].k,
                                       color='grey') 
                                       for prow, pcol in itertools.product(params[row], params[col])],
                       name=f'frame{t}', traces=list(range(n_plots))) 
              for t in range(n_frames)]
    step = perf_counter()
    print('step 2 : ', step-start, 's')
    start = step
    updatemenus = [dict(type='buttons',
                    buttons=[dict(label='Play',
                                  method='animate',
                                  args=[[f'frame{t}' for t in range(n_frames)],
                                        dict(frame=dict(duration=100, redraw=True),
                                             transition=dict(duration=0),
                                             easing='linear',
                                             fromcurrent=True,
                                             mode='immediate'
                                             )]),
                             dict(label='Pause',
                                  method='animate',
                                  args=[[None],
                                        dict(frame=dict(duration=0, redraw=False),
                                             transition=dict(duration=0),
                                             mode='immediate'
                                             )])
                             ],
                    direction='left',
                    pad=dict(r=10, t=85),
                    showactive=True, x=0.1, y=0, xanchor='right', yanchor='top')
               ]

    # Slider
    sliders = [{'yanchor': 'top',
                'xanchor': 'left',
                'currentvalue': {'font': {'size': 16}, 'prefix': 'Frame: ', 'visible': True, 'xanchor': 'right'},
                'transition': {'duration': 0, 'easing': 'linear'},
                'pad': {'b': 10, 't': 50},
                'len': 0.9, 'x': 0.1, 'y': 0,
                'steps': [{'args': [[f'frame{k}'], {'frame': {'duration': 0, 'easing': 'linear', 'redraw': True},
                                        'transition': {'duration': 0, 'easing': 'linear'}}],
                        'label': k, 'method': 'animate'} for k in range(n_frames)
                        ]}]

    zeroline=False
    scene_dict = dict(xaxis = dict(showgrid=False,
                                  zeroline=zeroline,
                                  visible=show_axes),
                     yaxis = dict(showgrid=False,
                                  zeroline=zeroline,
                                  visible=show_axes),
                     zaxis = dict(showgrid=False,
                                  zeroline=zeroline,
                                  visible=show_axes),
                    aspectmode='data')
    step = perf_counter()
    print('step 3 : ', step-start, 's')
    start = step

    fig.update(frames=frames),
    step = perf_counter()
    print('step 4 : ', step-start, 's')
    start = step
    fig.update_layout(updatemenus=updatemenus,
                      sliders=sliders)
    fig.update_layout(**{'scene'+(f'{i+1}' if i > 0 else ''):deepcopy(scene_dict) for i in range(n_plots)})

    step = perf_counter()
    print('step 5 : ', step-start, 's')
    print(200*'-')

    return fig

def create_app():
    app = Dash(__name__)
    
    app.radios = {o:dcc.RadioItems([{'label':f'{i:.1e}', 'value':i} for i in params[o]], inline=True, id=f'{o}_selector', value=v) for o,v in default_params.items()}
    app.checkboxes = {o:dcc.Checklist([{'label':f'{i:.1e}', 'value':i} for i in params[o]], inline=True, id=f'{o}_selector', value=v) for o,v in default_params.items()}
    app.selectors = app.radios.copy()
    app.selectors[OPTIONS[0]] = app.checkboxes[OPTIONS[0]]
    app.selectors[OPTIONS[1]] = app.checkboxes[OPTIONS[1]]

    fig = display_meshgrid(meshgrid)
    app.layout = html.Div([
        html.H4('Protoype'),
        dcc.Graph(id="graph",
            style={'display':'inline-block', 'vertical-align':'top', 'height':'90vh', 'width':'90vh'}),
        html.Div([
            html.Div([html.H5('Row param'),
                    dcc.RadioItems([{'label':o, 'value':o} for o in OPTIONS], value=OPTIONS[0], id='row_radio')],
                id='div_row_param',
                style={'display':'inline-block', 'vertical-align':'top', 'width':'fit-content', 'padding-right':'20px'}),
            html.Div([html.H5('Col param'),
                    dcc.RadioItems([{'label':o, 'value':o} for o in OPTIONS], value=OPTIONS[1], id='column_radio')],
                id='div_col_param',
                style={'display':'inline-block', 'vertical-align':'top', 'width':'fit-content'}),
            html.Div(list(itertools.chain(*[[html.H6(o), selector] for o, selector in app.selectors.items()])), id='selectors')
        ], id='div_control_panel', style={'display':'inline-block', 'vertical-align':'top', 'width':'fit-content'})
    ], style={'height': '90vh'})


    # to synchronize camera across plots /!\ high latency
    @app.callback(Output('graph', 'figure', allow_duplicate=True), Input('graph', 'relayoutData'),prevent_initial_call='initial_duplicate')
    def update_camera(relayout_data):
        fig = next([c for c in app.layout.children if c.id == "graph"])
        if relayout_data:
            cameras = [k for k in relayout_data if k.endswith('camera')]
            if cameras:
                camera = cameras.pop()
                for sc in [c for c in fig.layout if c.startswith('scene')]:
                    fig.layout[sc].camera = relayout_data[camera]

        return fig
    
    # callbacks to select row and column axes
    @app.callback([Output('column_radio', 'options'), Output('column_radio', 'value')], Input('row_radio', 'value'))
    def disable_row_axis_in_column_axis_list(value):
        div_control_panel = next(iter([c for c in app.layout.children if hasattr(c, 'id') and c.id=='div_control_panel']))
        div_col_param = next(iter([c for c in div_control_panel.children if hasattr(c, 'id') and c.id=='div_col_param']))
        options = div_col_param.children[-1].options
        for o in options:
            o['disabled'] = (o['value'] == value)
        col_radio = div_col_param.children[-1]
        value = None if col_radio.value == value else col_radio.value
        return options, value
        
    # callbacks to switch radios to checklist
    @app.callback(Output('selectors', 'children'), [Input('row_radio', 'value'), Input('column_radio', 'value')])
    def switch_checklists_to_radios(row, column):
        div_control_panel = next(iter([c for c in app.layout.children if hasattr(c, 'id') and c.id=='div_control_panel']))
        div_selectors = next(iter([c for c in div_control_panel.children if hasattr(c, 'id') and c.id=='selectors']))
        app.selectors = app.radios.copy()
        if row:
            app.selectors[row] = app.checkboxes[row]
        if column:
            app.selectors[column] = app.checkboxes[column]
        children = list(itertools.chain(*[[html.H6(o), selector] for o, selector in app.selectors.items()]))
        return children

    # callbacks to select values in the row and column using checklist
    @app.callback(Output('graph', 'figure', allow_duplicate=True), [Input(f'{o}_selector', 'value') for o in OPTIONS],prevent_initial_call='initial_duplicate')
    def update_meshgrid(*args):
        div_control_panel = next(iter([c for c in app.layout.children if hasattr(c, 'id') and c.id=='div_control_panel']))
        div_col_param = next(iter([c for c in div_control_panel.children if hasattr(c, 'id') and c.id=='div_col_param']))
        div_row_param = next(iter([c for c in div_control_panel.children if hasattr(c, 'id') and c.id=='div_row_param']))
        row = div_row_param.children[-1].value
        col = div_col_param.children[-1].value
        args = {k:(sorted(v) if isinstance(v, list) else v) for k,v in zip(OPTIONS, args)}
        div_row_param = next(iter([c for c in div_control_panel.children if hasattr(c, 'id') and c.id=='div_row_param']))
        fig = display_meshgrid(meshgrid, args, row=row, col=col)
        return fig


    return app



if __name__ == "__main__":
    app = create_app()
    app.run_server(debug=True)

