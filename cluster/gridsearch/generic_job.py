from utils import RangeParser, Indexer
from sofa.config import Config
from sofa.headless_launcher import launch_scene
from pathlib import Path

cfg = Config()
parser = RangeParser()
parser.add_argument('task_id', type=int)
parser.add_argument('subdir', type=str)
args = parser.parse_args()
indexer = Indexer(*[getattr(args, argname) for argname in args.range_args])

subdir = cfg.traj_type.lower()
for k, v in indexer[args.task_id].items():
    setattr(cfg, k, v)
    subdir += '_'+k+'_'+f'{v:.2e}'

cfg.recordSubdirectory = Path(args.subdir)/subdir

launch_scene(cfg)