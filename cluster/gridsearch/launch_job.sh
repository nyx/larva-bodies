#! /bin/sh

module load Python
source $HOME/workspace/larva-bodies/venv/bin/activate
python3 $HOME/workspace/larva-bodies/cluster/launch_job.py "$@"
sbatch run.sh
rm run.sh