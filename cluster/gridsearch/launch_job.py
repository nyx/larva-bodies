import os
from gridsearch.utils import RangeParser
from cluster.utils import get_timestamp
from math import prod

parser = RangeParser()
parser.add_argument('-t', '--traj_type', type=str)
args = parser.parse_args()
n_jobs = prod(getattr(args, range_arg).steps for range_arg in args.range_args)

env = f"--env TRAJ_TYPE={args.traj_type}"  if args.traj_type is not None else ""
apptainer_command = f"srun apptainer exec {env} $HOME/workspace/larva-bodies/cluster/larvabodies.sif  python3.10 $HOME/workspace/larva-bodies/cluster/gridsearch/generic_job.py"
apptainer_args = ["$SLURM_ARRAY_TASK_ID", get_timestamp()]
for range_arg in args.range_args:
    r = getattr(args, range_arg)
    apptainer_args.append(f"--{r.name} {r.start} {r.stop} {r.steps} {r.scale}")
with open ('run.sh', 'w') as rsh:
    rsh.write(f'''#! /bin/sh
#SBATCH --array 1-{n_jobs}
#SBATCH --cpus-per-task 4
#SBATCH -p dbc_pmo
#SBATCH --job-name larvasim{'_'.join(f"{r.name}_{r.start}_{r.stop}_{r.steps}_{r.scale}" for r in [getattr(args, range_arg) for range_arg in args.range_args])}
#SBATCH --mail-type BEGIN,END,FAIL

module load apptainer
{apptainer_command} {' '.join(apptainer_args)}
''')