import argparse
from math import exp, log
from typing import List
from sofa.config import Config

# RangeArgument and RangeParser

class RangeArgument:
    def __init__(self, name):
        self.nargs = 0
        self.name = name
        self.start = None
        self.stop = None
        self.steps = None
        self.scale = None
    def __call__(self, arg):
        match self.nargs:
            case 0:
                self.start = float(arg)
                self.nargs += 1
            case 1:
                self.stop = float(arg)
                self.nargs +=1
            case 2:
                self.steps = int(arg)
                self.nargs +=1
                assert(self.steps >= 1)
                self.scale = 'lin'
            case 3:
                self.scale = arg
                self.nargs += 1
                assert(self.scale in ['lin', 'log'])
                if self.scale == 'log':
                    assert(min(self.start, self.stop) > 0)
            case _:
                raise ValueError(f'Too many arguments passed for {self.name}')
        return self
    
    @property
    def range(self):
        return [self.value(i) for i in range(self.steps)]

    def value(self, i):
        if self.scale == 'lin':
            return self.start + i/(self.steps-1)*(self.stop-self.start)
        else:
            return exp(log(self.start) + i/(self.steps-1)*(log(self.stop)-log(self.start)))

    def __repr__(self):
        return f"Range({self.start},{self.stop},{self.steps},{self.scale})"

class RangeParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for arg in Config.float_args():
            self.add_range_argument(arg)

    def add_range_argument(self, name):
        self.add_argument(f'--{name}', type=RangeArgument(name), nargs="*", help='accepts 3-4 arguments : start stop steps [lin|log]', metavar='arg')
        if hasattr(self, 'range_args'):
            self.range_args.append(name)
        else:
            self.range_args = [name]
    def parse_args(self):
        args = super().parse_args()
        args.range_args = []
        for range_arg in self.range_args:
            if hasattr(args, range_arg) and getattr(args, range_arg) is not None:
                setattr(args, range_arg, getattr(args, range_arg)[-1])
                args.range_args.append(range_arg)

        return args
    
# Indexer
    
class Indexer:
    def __init__(self, *range_args:List[RangeArgument]):
        self.range_args = range_args

    def __getitem__(self, index):
        args = dict()
        for range_arg in self.range_args:
            index, remainder = index//range_arg.steps, index%range_arg.steps
            args[range_arg.name] = range_arg.value(remainder)
        return args

