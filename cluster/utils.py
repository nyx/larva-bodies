def get_timestamp() -> str:
    import datetime
    return datetime.datetime.now(datetime.timezone.utc).strftime('%d%m%Y_%H%M%S')