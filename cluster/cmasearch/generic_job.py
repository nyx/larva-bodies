from sofa.config import Config
from sofa.headless_launcher import launch_scene
from pathlib import Path
from argparse import ArgumentParser, Namespace



cfg = Config()
parser = ArgumentParser()
parser.add_argument('subdir', type=str)

# subdir = cfg.traj_type.lower()

for arg in Config.float_args():
    parser.add_argument('--'+arg, default=cfg.__dict__[arg], type=float)

# default_handling overrides the default values, so that only values passed as arguments are used for the subfolder name
default_handling = Namespace(**{key:None for key in Config.float_args()})    
args = parser.parse_args(namespace=default_handling)

for arg in Config.float_args():
    val = args.__dict__[arg]
    if val:
        setattr(cfg, arg, val)
        # subdir += '_'+arg+'_'+f'{val:.2e}'

cfg.steps = 2
cfg.recordSubdirectory = Path(args.subdir)

launch_scene(cfg)