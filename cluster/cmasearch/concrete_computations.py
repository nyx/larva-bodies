import os
import objectives
import subprocess
from pathlib import Path
import cma
import numpy as np
from analysis.outline import compute_outline
from analysis.current import current_based_distance_squared
from sofa.config import default_cfg
import matplotlib.pyplot as plt


def launch_solution_computation(candidate:dict, step:int, sample:int, dir:Path=Path(''), traj_type='ROLL')->str:
    env = f"--env TRAJ_TYPE={traj_type}" if traj_type is not None else ""
    apptainer_command = f"srun apptainer exec {env} $HOME/workspace/larva-bodies/cluster/larvabodies.sif  python3.10 $HOME/workspace/larva-bodies/cluster/cmasearch/generic_job.py"
    apptainer_args = [str(dir/f"step_{step}"/"output_$SLURM_JOB_ID")]
    for argName, argVal in candidate.items():
        apptainer_args.append(f"--{argName} {argVal}")

    with open ('run.sh', 'w') as rsh:
        rsh.write( "#!/bin/sh\n"
                 + "#SBATCH --cpus-per-task 4\n"
                 + "#SBATCH -p dbc_pmo\n"
                 +f"#SBATCH --job-name larvasim_{'_'.join(f'{argName}_{argVal}' for argName, argVal in candidate.items())}\n"
                 + "module load apptainer\n"
                 + "echo $SLURM_JOB_NAME\n"
                 + "echo $SLURM_JOB_ID\n"
                 +f"{apptainer_command} {' '.join(apptainer_args)}")

    job_id = subprocess.run('sbatch --parsable run.sh', shell=True, check=True, stdout=subprocess.PIPE).stdout
    job_id = job_id.decode().strip()
    return job_id

def is_finished(job_id:str)->float:
    output = subprocess.run(f"sacct -j {job_id} --format=JobID,State --parsable", shell=True, check=True, stdout=subprocess.PIPE).stdout.decode()
    lines = [l.strip('\n|').split('|') for l in output.strip().split('\n')][1:]
    job_idx = [id for id, status in lines].index(job_id)
    _, status = lines[job_idx]
    
    return status in ['COMPLETED', 'FAILED']

def objective(experiment_dir, step:int, job_id:str)->float:
    # get the reference outlines
    path_ro_ref = Path(__file__).parent/'target_larva.npz'
    ref = np.load(path_ro_ref)
    ref_times = ref['t'][0]
    ref_times = ref_times - ref_times[0]
    ref_xc, ref_yc = ref['x_contour'], ref['y_contour']
    ref_outlines = [1e-3*np.stack([x[np.logical_not(np.isnan(x))], y[np.logical_not(np.isnan(y))]]).transpose()  for x, y in zip(ref_xc.transpose() , ref_yc.transpose() )]
    ref_outlines = ref_outlines[:2] # debug
    ref_times = ref_times[:2] # debug

    # realign the reference outlines

    # initial_outline = ref_outlines[0]
    # center = initial_outline.mean(axis=0)
    # eigenvector = np.linalg.eigh(initial_outline.cov(axis=0))
    # eigenvector = eigenvector/np.linalg.norm(eigenvector)
    # if eigenvector[0] < 0: eigenvector = -eigenvector
    # c,s = eigenvector
    # R = np.array([[c, -s],
    #               [s,  c]])

    # get data from the run
    path_to_data = default_cfg.recordDirectory/experiment_dir/f"step_{step}"/f"output_{job_id}"/'records.npz'
    data = np.load(path_to_data)
    positions, edges, tets = data['body_positions'], data['edges'], data['tets']
    times = data['times']

    # subsample data from the run to match recorded data
    subsampled_times = np.searchsorted(times, ref_times)
    subsampled_times = subsampled_times[subsampled_times<len(times)] # debug
    # print(times)
    # print(ref_times)
    # print(subsampled_times)
    times = times[subsampled_times]
    positions = positions[subsampled_times]

    # compute outlines
    outlines = []
    for coords in positions:
        outlines.append(compute_outline(coords, edges, tets, return_type='vector'))

    print('outline shape :', outlines[0].shape)

    plt.figure()
    o1 = outlines[0]
    o1 = o1-o1.mean(axis=0,keepdims=True)
    o2 = ref_outlines[0]
    print(o2.shape, 'shape of ref')
    o2 = o2-o2.mean(axis=0,keepdims=True)
    plt.plot(o1[:,0], o1[:,1], 'g')
    plt.plot(o2[:,0], o2[:,1], 'r')
    plt.savefig(default_cfg.recordDirectory/experiment_dir/f"step_{step}"/f"output_{job_id}"/f"outlines_{step}.pdf")
    plt.close()
    # register by procrustes
    
    # compute the distance
    # TODO TODO TODO TODO
    sum_squared_errors = 0
    for o1, o2 in zip(ref_outlines, outlines):
        sum_squared_errors += current_based_distance_squared(o1-o1.mean(axis=0,keepdims=True), o2-o2.mean(axis=0,keepdims=True), sigma=1e-3)
    print(job_id,':',sum_squared_errors)
    return sum_squared_errors