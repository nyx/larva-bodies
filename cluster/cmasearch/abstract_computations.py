import cma

def launch_solution_computation(candidate):
    return candidate

def is_finished(job_name):
    return True

def objective(job_name):
    return cma.ff.rosen(job_name)