import cma
from time import sleep
from cluster.utils import get_timestamp
from pathlib import Path


from concrete_computations import launch_solution_computation, is_finished, objective

def proposal_to_parameters(candidate):
    params = {
        'Fmax':10**(-1.5+0.5*candidate[0]/3),
        'rayleighStiffness':10**(-2+2*candidate[1]/3),
        'youngModulus':10**(8+candidate[2]/3),
    }
    return params
    
def main():
    experiment_dir = Path(f"cmasearch_{get_timestamp()}")
    es = cma.CMAEvolutionStrategy(3 * [0], 1)
    step = 0
    while not es.stop():
        candidates = es.ask(number=10)
        job_ids = [launch_solution_computation(proposal_to_parameters(c),
                                                 step,
                                                 sample,
                                                 experiment_dir) for sample, c in enumerate(candidates)]
        while True:
            sleep(60)
            if all([is_finished(job_id) for job_id in job_ids]):
                break
        print("lengths  ", len(candidates), len(job_ids))
        es.tell(candidates, [objective(experiment_dir, step, job_id) for job_id in job_ids])
        es.logger.add()  # write data to disc to be plotted
        es.disp()
        step += 1
    es.result_pretty()
    # cma.plot()


if __name__ == '__main__':
    main()