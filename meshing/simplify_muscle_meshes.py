import open3d
import numpy as np
import scipy.spatial as spatial
import random
from sklearn.manifold import Isomap, LocallyLinearEmbedding
import itertools
import matplotlib.pyplot as plt

import os
import time

import torch
from torch.autograd import grad

import plotly.graph_objs as go

from pykeops.torch import Vi, Vj

# torch type and device
use_cuda = torch.cuda.is_available()
torchdeviceId = torch.device("cuda:0") if use_cuda else "cpu"
torchdtype = torch.float32

# PyKeOps counterpart
KeOpsdeviceId = torchdeviceId.index  # id of Gpu device (in case Gpu is  used)
KeOpsdtype = torchdtype.__str__().split(".")[1]  # 'float32'

def compute_registration_transform(verts, permute=False):
    mean = np.mean(verts, axis=0, keepdims=True)
    cov = np.cov(verts.transpose())
    lam, R = np.linalg.eigh(cov)
    lam, R = lam[::-1], R[:,::-1]
    if permute:
        R = R[:,[0,2,1]]
        lam = lam[[0,2,1]]

    
    def registration_transform(X):
        '''
        X is of shape (N,3)
        '''
        X = X.copy()
        X = X-np.mean(X, axis=0)                          # center X

        X = X@np.diag(np.sqrt(lam))                       # scale X
        X = 3*X@R.transpose()                               # rotate X
        X = X+mean                                        # shift X
        return X
    
    return registration_transform

def get_initial_guess(nx, ny, verts):
    initial_guess = np.stack(list(np.meshgrid(np.linspace(0,1,nx),
                                        np.linspace(0,1,ny),
                                        indexing='ij')) 
                                        +[np.zeros((nx,ny))],
                            axis=-1)
    initial_guess = initial_guess.reshape(-1,3)


    templateFaces =  ([[(i,j), (i+1,j), (i+1,j+1)] for i, j in itertools.product(range(nx-1), range(ny-1))]
                    + [[(i,j), (i,j+1), (i+1,j+1)] for i, j in itertools.product(range(nx-1), range(ny-1))])
    for f in templateFaces:
        for i in range(3):
            f[i] = f[i][0]*ny + f[i][1]
    return initial_guess, templateFaces

def GaussKernel(sigma):
    x, y, b = Vi(0, 3), Vj(1, 3), Vj(2, 3)
    gamma = 1 / (sigma * sigma)
    D2 = x.sqdist(y)
    K = (-D2 * gamma).exp()
    return (K * b).sum_reduction(axis=1)

def GaussLinKernel(sigma):
    x, y, u, v, b = Vi(0, 3), Vj(1, 3), Vi(2, 3), Vj(3, 3), Vj(4, 1)
    gamma = 1 / (sigma * sigma)
    D2 = x.sqdist(y)
    K = (-D2 * gamma).exp() * (u * v).sum() ** 2
    return (K * b).sum_reduction(axis=1)

def RalstonIntegrator():
    def f(ODESystem, x0, nt, deltat=1.0):
        x = tuple(map(lambda x: x.clone(), x0))
        dt = deltat / nt
        l = [x]
        for i in range(nt):
            xdot = ODESystem(*x)
            xi = tuple(map(lambda x, xdot: x + (2 * dt / 3) * xdot, x, xdot))
            xdoti = ODESystem(*xi)
            x = tuple(
                map(
                    lambda x, xdot, xdoti: x + (0.25 * dt) * (xdot + 3 * xdoti),
                    x,
                    xdot,
                    xdoti,
                )
            )
            l.append(x)
        return l

    return f

def Hamiltonian(K):
    def H(p, q):
        return 0.5 * (p * K(q, q, p)).sum()

    return H


def HamiltonianSystem(K):
    H = Hamiltonian(K)

    def HS(p, q):
        Gp, Gq = grad(H(p, q), (p, q), create_graph=True)
        return -Gq, Gp

    return HS

def Shooting(p0, q0, K, nt=10, Integrator=RalstonIntegrator()):
    return Integrator(HamiltonianSystem(K), (p0, q0), nt)


def Flow(x0, p0, q0, K, deltat=1.0, Integrator=RalstonIntegrator()):
    HS = HamiltonianSystem(K)

    def FlowEq(x, p, q):
        return (K(x, q, p),) + HS(p, q)

    return Integrator(FlowEq, (x0, p0, q0), deltat)[0]


def LDDMMloss(K, dataloss, gamma=0):
    def loss(p0, q0):
        p, q = Shooting(p0, q0, K)[-1]
        return gamma * Hamiltonian(K)(p0, q0) + dataloss(q)

    return loss

# VT: vertices coordinates of target surface,
# FS,FT : Face connectivity of source and target surfaces
# K kernel
def lossVarifoldSurf(FS, VT, FT, K):
    def get_center_length_normal(F, V):
        V0, V1, V2 = (
            V.index_select(0, F[:, 0]),
            V.index_select(0, F[:, 1]),
            V.index_select(0, F[:, 2]),
        )
        centers, normals = (V0 + V1 + V2) / 3, 0.5 * torch.cross(V1 - V0, V2 - V0)
        length = (normals**2).sum(dim=1)[:, None].sqrt()
        return centers, length, normals / length

    CT, LT, NTn = get_center_length_normal(FT, VT)
    cst = (LT * K(CT, CT, NTn, NTn, LT)).sum()

    def loss(VS):
        CS, LS, NSn = get_center_length_normal(FS, VS)
        return (
            cst
            + (LS * K(CS, CS, NSn, NSn, LS)).sum()
            - 2 * (LS * K(CS, CT, NSn, NTn, LT)).sum()
        )

    return loss

def get_deformed_coordinates(verts, faces, templateVerts, templateFaces, verbose=False, sig_vs=0.005):
    def log(*args, **kwargs):
        if verbose:
            print(*args, **kwargs)
    VT, FT, VS, FS = torch.tensor(verts), torch.tensor(faces), torch.tensor(templateVerts), torch.tensor(templateFaces)
    q0 = VS.clone().detach().to(dtype=torchdtype, device=torchdeviceId).requires_grad_(True)
    VT = VT.clone().detach().to(dtype=torchdtype, device=torchdeviceId)
    FS = FS.clone().detach().to(dtype=torch.long, device=torchdeviceId)
    FT = FT.clone().detach().to(dtype=torch.long, device=torchdeviceId)

    p0 = torch.zeros(q0.shape, dtype=torchdtype, device=torchdeviceId, requires_grad=True)
    start = time.time()

    sigma_varisurf = torch.tensor([sig_vs], dtype=torchdtype, device=torchdeviceId)
    sigma_lddmm = torch.tensor([.02], dtype=torchdtype, device=torchdeviceId)
    dataloss = lossVarifoldSurf(FS, VT, FT, GaussLinKernel(sigma=sigma_varisurf))
    Kv = GaussKernel(sigma=sigma_lddmm)
    loss = LDDMMloss(Kv, dataloss, gamma=1e-5)

    # initialize momentum vectors
    steps = 10
    optimizer = torch.optim.LBFGS([p0], max_eval=steps, max_iter=steps)
    log("performing optimization...")


    def closure():
        optimizer.zero_grad()
        L = loss(p0, q0)
        log("loss", L.detach().cpu().numpy())
        L.backward()
        return L


    for i in range(steps):
        log("it ", i, ": ", end="")
        optimizer.step(closure)
        final_loss = loss(p0, q0).detach().cpu().numpy()

    log("Optimization (L-BFGS) time: ", round(time.time() - start, 2), " seconds")
    qfinal = Shooting(p0, q0, Kv, nt=1)[-1][1].detach().cpu().numpy()

    return qfinal, final_loss

# file list
dir = "mechanical_larva/assets/meshes"
files = []
for fn in os.listdir(os.path.join(dir, 'muscle_meshes')):
    if not(fn.startswith('mesh_') and fn.endswith('.obj')):
        continue
    files.append(fn)
files.sort()


def main(file_list, param_dict={}, save=True, plot=False):
    start_time = time.time()
    for i, fn in enumerate(file_list):
        current_time = time.time() - start_time
        if i:
            print(f'{i}/{len(file_list)} | avg time {current_time/i} | elapsed {current_time} | proj. total {len(file_list)/i*current_time} | remaining {(len(file_list)-i)/i*current_time}')
        meshfile = os.path.join(dir, 'muscle_meshes', fn)
        mesh = open3d.io.read_triangle_mesh(meshfile)

        verts = np.asarray(mesh.vertices)
        faces = np.asarray(mesh.triangles, dtype=int)

        nx, ny = 100, 50


        tr1 = compute_registration_transform(verts)
        tr2 = compute_registration_transform(verts, True)

        initial_guess, templateFaces = get_initial_guess(nx, ny, verts)

        ############################
        # Surface registration
        ##########################
        sig_vs = param_dict[fn] if fn in param_dict else 0.005
        if not(isinstance(sig_vs, list)):
            sig_vs = [sig_vs]

        qf1 = tr1(initial_guess)
        for s in sig_vs:
            qf1, loss1 = get_deformed_coordinates(verts, faces, qf1, templateFaces, sig_vs=s)


        qf2 = tr2(initial_guess)
        for s in sig_vs:
            qf2, loss2 = get_deformed_coordinates(verts, faces, qf2, templateFaces, sig_vs=s)


        if loss1 < loss2:
            qf, loss = qf1, loss1
        else:
            qf, loss = qf2, loss2
        # print(loss1, loss2)

        templateVerts = qf

        simplified_mesh = open3d.geometry.TriangleMesh(open3d.utility.Vector3dVector(templateVerts), open3d.utility.Vector3iVector(templateFaces))
        simplified_meshfile = os.path.join(dir, 'simplified_muscle_meshes', 'simplified_'+os.path.basename(meshfile))
        if save:
            open3d.io.write_triangle_mesh(simplified_meshfile, simplified_mesh)

        nycable = 5
        cableverts = templateVerts.reshape(nx, ny, 3)
        
        xs = nx//2
        ys = np.linspace(0, ny-1, nycable).astype(int)
        cableVerts1 = cableverts[xs,ys]
        len1 = np.sum(np.linalg.norm(np.diff(cableVerts1, axis=0), axis=1))

        xs = np.linspace(0, nx-1, nycable).astype(int)
        ys = ny//2
        cableVerts2 = cableverts[xs,ys]
        len2 = np.sum(np.linalg.norm(np.diff(cableVerts2, axis=0), axis=1))
        
        cableVerts = cableVerts1 if len1 > len2 else cableVerts2
        cablefile = os.path.join(dir, 'cable_muscle_meshes', 'npy', 'cable'+os.path.basename(meshfile)[:-4]+'.npy')
        if save:
            np.save(cablefile, cableVerts)

            cablefile = os.path.join(dir, 'cable_muscle_meshes', 'obj', 'cable'+os.path.basename(meshfile)[:-4]+'.obj')
            f = open(cablefile, 'w')
            for x, y, z in cableVerts:
                f.write(f'v {x} {y} {z}\n')
            f.write('l '+' '.join([str(i) for i in range(1, len(cableVerts)+1)])+'\n')
            f.close()

        if plot:
            # open3d.visualization.draw_geometries([ig1, ig2, mesh, simplified_mesh])
            open3d.visualization.draw_geometries([mesh, simplified_mesh])


if __name__ == '__main__':
    param_dict = {'mesh_123.obj':0.008,
                  'mesh_203.obj':0.007,
                  'mesh_208.obj':0.01,
                  'mesh_274.obj':0.003,
                  'mesh_303.obj':0.01,
                  'mesh_353.obj':0.01,
                  'mesh_359.obj':0.003,
                  'mesh_360.obj':0.003,
                  'mesh_469.obj':0.01,
                  'mesh_509.obj':0.003,
                  'mesh_530.obj':[0.003, 0.01],
                  'mesh_569.obj':0.01,
                  'mesh_572.obj':0.004,
                  'mesh_604.obj':0.003,
                  'mesh_609.obj':0.003,
                  'mesh_659.obj':0.003,
                  'mesh_704.obj':[0.01, 0.005],
                  'mesh_859.obj':0.004,
                  'mesh_867.obj':0.01,
                  'mesh_1002.obj':[0.01, 0.003]}
    main(files, param_dict=param_dict)
