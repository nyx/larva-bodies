import tifffile
import numpy as np
import trimesh
import open3d
import os
from skimage import measure, morphology, transform


def cleanup(mesh):
    mesh.remove_duplicated_triangles()
    mesh.remove_duplicated_vertices()
    mesh.remove_degenerate_triangles()
    mesh.remove_unreferenced_vertices()
    mesh.remove_non_manifold_edges()
    triangle_clusters, cluster_n_triangles, _ = mesh.cluster_connected_triangles()
    triangle_clusters = np.asarray(triangle_clusters)
    cluster_n_triangles = np.asarray(cluster_n_triangles)
    triangles_to_remove = cluster_n_triangles[triangle_clusters] < cluster_n_triangles.max()
    mesh.remove_triangles_by_mask(triangles_to_remove)

def get_verts_and_faces(im, level, verbose=False):
    if verbose:
        print('mesh creation')
    verts, faces, _, _ = measure.marching_cubes(im, level)
    return verts, faces

def mesh_from_verts_and_faces(verts, faces, filter=False, simplification=None, simplification_factor=None, verbose=False):
    mesh = open3d.geometry.TriangleMesh(vertices=open3d.utility.Vector3dVector(verts), triangles=open3d.utility.Vector3iVector(faces))
    if filter:
        if verbose:
            print('filtering')
        mesh = mesh.filter_smooth_laplacian(number_of_iterations=10)
    cleanup(mesh)
    if simplification is not None:
        if verbose:
            print('simplification')
        mesh = mesh.simplify_quadric_decimation(target_number_of_triangles=min(simplification, len(faces)))
    elif simplification_factor is not None:
        if verbose:
            print('simplification')
            if len(faces) > 100:
                target_number_of_triangles = int(len(faces)//simplification_factor)
                mesh = mesh.simplify_quadric_decimation(target_number_of_triangles=target_number_of_triangles)
    cleanup(mesh)
    # TODO to further reduce artifacts, implement erosion + cluster detection + closing

    mesh.compute_triangle_normals()
    mesh.compute_vertex_normals()

    return mesh

def downscale(im, downscaling=8):
    if downscaling > 1:
        im = transform.downscale_local_mean(im, (downscaling,downscaling,downscaling))
    return im

def compute_registration_transform():
    im = tifffile.imread("mechanical_larva/assets/larva-micro-ct5.tif")
    verts, _, _, _ = measure.marching_cubes(im, 1)
    mean = np.mean(verts, axis=0, keepdims=True)
    cov = np.cov(verts.transpose())
    x_axis = np.linalg.eigh(cov)[1][:,-1]
    x_axis /= np.linalg.norm(x_axis)

    projection_x = np.dot(verts-mean, x_axis)
    length = np.max(projection_x) - np.min(projection_x)

    # r1 realigns the body along the x axis
    r1_a = np.cross(x_axis, np.array([1,0,0]))
    r1_k = (r1_a/np.linalg.norm(r1_a)).reshape(-1,1)
    r1_c = np.dot(x_axis, np.array([1,0,0]))

    I = np.eye(3)
    r1 = r1_c*I + np.cross(I, r1_a) + (1-r1_c)*r1_k*r1_k.transpose()

    # r2 rotates the larva upright
    deg = 2*np.pi/360
    r2_th = 66*deg # 66° calibrate visually
    r2_c = np.cos(r2_th)
    r2_s = np.sin(r2_th)
    r2_k = np.array([1,0,0]).reshape(-1,1)

    r2 = r2_c*I + r2_s*np.cross(I, r2_k.flatten()) + (1-r2_c)*r2_k*r2_k.transpose()


    r = r2@r1

    def registration_transform(X):
        '''
        X is oh shape (N,3)
        '''
        X = X.copy()
        X = (X-mean)/length
        X = X@r.transpose()
        return X
    
    return registration_transform


def mesh_muscles(registration_transform=None):
    im = tifffile.imread("mechanical_larva/assets/Final_larva_Frebruary2024.tif")

    meshes = []
    for lbl in np.unique(im):
        print('label ', lbl)
        if not(lbl):
            continue
        verts, faces = get_verts_and_faces(im==lbl, level=.5, verbose=True)
        verts = verts + np.array([[760,0,0]])
        verts = registration_transform(verts)
        mesh = mesh_from_verts_and_faces(verts, faces, filter=True,
                             simplification_factor=10, verbose=True)
        open3d.io.write_triangle_mesh(f"mechanical_larva/assets/meshes/mesh_{lbl}.obj", mesh)
        meshes.append(mesh)

    open3d.visualization.draw_geometries(meshes)

def show_muscles(registration_transform=None, cuticle=False):
    dir = "mechanical_larva/assets/meshes"

    meshes = []
    for f in os.listdir(os.path.join(dir, 'muscle_meshes')):
        if not(f.startswith('mesh_') and f.endswith('.obj')):
            continue
        mesh = open3d.io.read_triangle_mesh(os.path.join(dir, 'muscle_meshes', f))
        meshes.append(mesh)
    if cuticle:
        mesh = open3d.io.read_triangle_mesh(os.path.join(dir, 'cuticle_simplified.obj'))
        meshes.append(mesh)

    open3d.visualization.draw_geometries(meshes)

def mesh_cuticle(registration_transform=None):
    downscaling = 8
    simplification = 20000
    im = tifffile.imread("mechanical_larva/assets/larva-micro-ct5.tif")
    im = downscale(im, downscaling)
    verts, faces = get_verts_and_faces(im, 1, verbose=True)
    verts = verts*downscaling
    verts = registration_transform(verts)
    mesh = mesh_from_verts_and_faces(verts, faces, filter=True, simplification=simplification, verbose=True)
    mesh_frame = open3d.geometry.TriangleMesh.create_coordinate_frame(
    size=1, origin=[0,0,0])
    open3d.visualization.draw_geometries([mesh, mesh_frame])
    open3d.io.write_triangle_mesh('mechanical_larva/assets/meshes/cuticle_simplified.obj', mesh)

def mesh_interior(registration_transform=None):
    downscaling = 8
    simplification = 20000
    im = tifffile.imread("mechanical_larva/assets/larva-micro-ct5.tif")
    im = downscale(im, downscaling)
    verts, faces = get_verts_and_faces(im, 1, verbose=True)
    verts = verts*downscaling
    verts = registration_transform(verts)
    mesh1 = mesh_from_verts_and_faces(verts, faces, filter=True, simplification=simplification, verbose=True)

    # TODO to further reduce artifacts, implement erosion + cluster detection + closing

    print('morphology')
    im = morphology.erosion(im, morphology.ball(50//downscaling))
    verts, faces = get_verts_and_faces(im, 1, verbose=True)
    verts = verts*downscaling
    verts = registration_transform(verts)
    mesh2 = mesh_from_verts_and_faces(verts, faces, filter=True, simplification=simplification, verbose=True)

    open3d.visualization.draw_geometries([mesh1, mesh2])

    open3d.io.write_triangle_mesh('mechanical_larva/assets/meshes/cuticle_simplified.obj', mesh1)
    open3d.io.write_triangle_mesh('mechanical_larva/assets/meshes/inner_simplified.obj', mesh2)
    
def unroll_mesh(mesh, shift=0):
    verts = np.array(mesh.vertices)
    faces = np.array(mesh.triangles)
    center = np.array([0.025,0]).reshape(1,-1)
    centered_verts = verts[:,1:]-center
    r = np.linalg.norm(centered_verts[:,1:], axis=1)
    theta = (np.arctan2(centered_verts[:,0], centered_verts[:,1])-np.pi/2+shift)%(2*np.pi)-np.pi

    # correct muscles crossing the dissection line
    if np.any(theta<-np.pi/2) and np.any(theta>np.pi/2):
        s = np.sign(np.sum(theta>0)-np.sum(theta<0))
        if s>0:
            theta[theta<-np.pi/2] += 2*np.pi
        else:
            theta[theta>np.pi/2] += -2*np.pi
    verts[:,1] = theta*8.4e-2
    verts[:,2] = r
    return open3d.geometry.TriangleMesh(vertices=open3d.utility.Vector3dVector(verts), triangles=open3d.utility.Vector3iVector(faces))


def unroll_meshes(plot=True, save=True):
    src = "mechanical_larva/assets/meshes"
    dest = '/home/alexandre/Desktop/test_larva'
    top_cut_meshes, bottom_cut_meshes = [], []
    for f in os.listdir(os.path.join(src, 'muscle_meshes')):
        if not(f.startswith('mesh_') and f.endswith('.obj')):
            continue
        mesh = open3d.io.read_triangle_mesh(os.path.join(src, 'muscle_meshes', f))
        top_cut_mesh = unroll_mesh(mesh)
        top_cut_meshes.append(top_cut_mesh)
        bottom_cut_mesh = unroll_mesh(mesh, shift=np.pi)
        bottom_cut_meshes.append(bottom_cut_mesh)
        if save:
            open3d.io.write_triangle_mesh(os.path.join(dest, 'top_cut', f), mesh)
            open3d.io.write_triangle_mesh(os.path.join(dest, 'bottom_cut', f), mesh)
    if plot:
        open3d.visualization.draw_geometries(top_cut_meshes)
        open3d.visualization.draw_geometries(bottom_cut_meshes)

if __name__ == '__main__':
    registration_transform = compute_registration_transform()
    # mesh_muscles(registration_transform)
    # show_muscles(cuticle=True)
    unroll_meshes()
    # mesh_cuticle(registration_transform)
    # mesh_interior(registration_transform)