import numpy as np
from time import time
from pathlib import Path

root = Path(__file__).parent
filename = str(root/'larva_straightener.obj')
meshes_path = root.parent/'assets'/'meshes'
with open(filename, 'r') as f:
    lines = f.readlines()

objects = {}
object = []
objectName = None
for l in lines:
    if l.startswith('o'):
        if objectName is not None:
            objects[objectName] = object
        objectName = l[2:].strip(' \n')
        object = []
    elif objectName is not None:
        object.append(l)
if objectName is not None:
    objects[objectName] = object

print(len(objects))
print('Press any key.')
input()

for objectName, object in objects.items():
    print(objectName)
    if not objectName.startswith('cable') and not objectName == 'armature_mesh':
        continue

    if objectName == 'armature_mesh':
        dest = str(meshes_path/'larva_midline')
    else:
        dest = str(meshes_path/'corrected_cable_muscle_meshes')

    edges = []
    for l in object:
        if l.startswith('l'):
            l = l[2:-1].split(' ')
            edges.append((int(l[0]), int(l[1])))

    print('ordering...')
    edges_init = edges.copy()
    ordering = list(edges.pop())

    while len(edges) > 0:            
        for (e1, e2) in reversed(edges):
            if e1 == ordering[0]:
                ordering.insert(0,e2)
                edges.remove((e1,e2))
            elif e2 == ordering[0]:
                ordering.insert(0,e1)
                edges.remove((e1,e2))
            elif e1 == ordering[-1]:
                ordering.append(e2)
                edges.remove((e1,e2))
            elif e2 == ordering[-1]:
                ordering.append(e1)
                edges.remove((e1,e2))

    min_ordering = min(ordering)
    ordering = [i-min_ordering for i in ordering]

    print('saving...')
    new_obj = [object[i] for i in ordering]
    new_npy = np.array([[float(r) for r in l[2:-1].split(' ')] for l in new_obj])
    new_obj.append('l '+' '.join([str(i) for i in range(1, len(ordering)+1)])+'\n')

    obj_path = dest+'/obj/'+objectName+'.obj'
    with open(obj_path, 'w') as f:
        for l in new_obj:
            f.write(l)

    npy_path = dest+'/npy/'+objectName+'.npy'
    np.save(npy_path, new_npy)

# export the cuticle mesh
dest = str(root.parent/'assets'/'meshes'/'cuticle_meshes'/'cuticle_simplified_cleaned_straightened.obj')
cuticle = objects['cuticle_simplified']

normals = set()
vertices = set()
for l in cuticle:
    if l.startswith('f'):
        tokens = l.strip().split(' ')[1:]
        for tk in tokens:
            v, n = tk.split('//')
            normals.add(n)
            vertices.add(v)
normals = list(normals)
normals.sort()
vertices = list(vertices)
vertices.sort()

for i, l in enumerate(cuticle):
    new_l = 'f'
    if l.startswith('f'):
        tokens = l.strip().split(' ')[1:]
        for tk in tokens:
            v, n = tk.split('//')
            v = vertices.index(v)+1
            n = normals.index(n)+1
            new_l += f' {v}//{n}'
        new_l += '\n'
        cuticle[i] = new_l

with open(dest, 'w') as f:
    f.write('o cuticle_simplified\n')
    for l in cuticle:
        f.write(l)