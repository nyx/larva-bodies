import bpy
import mathutils
import math

if bpy.context.object:
    bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='DESELECT')

bpy.ops.wm.collada_import(filepath='/home/alexandre/Desktop/larva_rig.dae')

armature = bpy.data.objects.get('Armature')


meshes = [obj.name for obj in bpy.data.objects if obj.type == 'MESH']


BONE_RADIUS = 0.08  # Desired constant radius for the head and tail of the bone
BONE_DISTANCE = 0.03  # Desired constant envelope distance

# Update bone distances to obtain proper weights

# Enter Edit Mode to modify bone properties
bpy.context.view_layer.objects.active = armature
armature.show_in_front = True
bpy.ops.object.mode_set(mode='EDIT')

# Access the armature data in edit mode
for bone in armature.data.edit_bones:
    bone.head_radius = 0.0 if bone.name == 'control bone' else BONE_RADIUS
    bone.tail_radius = 0.0 if bone.name == 'control bone' else BONE_RADIUS
    bone.envelope_distance = 0.0 if bone.name == 'control bone' else BONE_DISTANCE

# Unset deform for control bone
control_bone = armature.data.edit_bones.get('control bone')
control_bone.use_deform = False

# Return to Object Mode
bpy.ops.object.mode_set(mode='OBJECT')


# Parent meshes to the armature using envelope weights
for mesh_name in meshes:
    mesh = bpy.data.objects.get(mesh_name)
    
    # Deselect all objects
    bpy.ops.object.select_all(action='DESELECT')
    
    # Select the mesh and the armature
    mesh.select_set(True)
    armature.select_set(True)
    
    # Make the armature the active object
    bpy.context.view_layer.objects.active = armature
    
    # Parent the mesh to the armature using automatic weights
    bpy.ops.object.parent_set(type='ARMATURE_ENVELOPE')
    
    armature_modifier = next((mod for mod in mesh.modifiers if mod.type == 'ARMATURE'), None)
    if armature_modifier:
        # Enable Preserve Volume
        armature_modifier.use_deform_preserve_volume = True
        print(f"Preserve Volume enabled for '{mesh_name}'.")


# Set inverse kinematics constraints
bone_name = "Bone.01.front"          # Replace with the name of the bone to add IK
ik_target_name = "control bone"  # Replace with the name of the IK target object (e.g., an empty)
chain_length = 0  # Number of parent bones affected by the IK

# Enter Pose Mode
bpy.context.view_layer.objects.active = armature
bpy.ops.object.mode_set(mode='POSE')

# Get the pose bone to apply IK and the target bone
pose_bone = armature.pose.bones.get(bone_name)
ik_target = armature.pose.bones.get(ik_target_name)

# Add an IK constraint
if not(any(constraint.type == 'IK' for constraint in pose_bone.constraints)):
    ik_constraint = pose_bone.constraints.new(type='IK')
    ik_constraint.target = armature  # Set the IK target
    ik_constraint.subtarget = ik_target_name
    ik_constraint.chain_count = chain_length  # Set the chain length
    ik_constraint.use_rotation = True

    print(f"Added IK constraint to bone '{bone_name}' with chain length {chain_length}.")

# Return to Object Mode
bpy.ops.object.mode_set(mode='OBJECT')
    

# align the armature so the tail is on the x-axis
armature.location = mathutils.Vector((0, 0, 0))
bpy.context.view_layer.objects.active = armature
bpy.ops.object.mode_set(mode='EDIT')

tail_bone = armature.data.edit_bones.get("Bone.11.back")
bpy.context.view_layer.update()
head = armature.matrix_world @ tail_bone.head
head = head
z, y = head.z, head.y

bpy.ops.object.mode_set(mode='OBJECT')
armature.location.z = -z
armature.location.y = -y
bpy.context.view_layer.update()
print('hey')

# Move the control bone so that the mesh is straight
bpy.context.view_layer.objects.active = armature
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.transform_apply(location=True, rotation=True, scale=True)
bpy.ops.object.mode_set(mode='POSE')

control_bone = armature.pose.bones.get("control bone")
bpy.ops.pose.transforms_clear()
bpy.context.view_layer.update()
target_world_position = mathutils.Vector((0.7,0,0))
target_local_position = armature.matrix_world.inverted() @ target_world_position
control_bone.matrix.translation = target_local_position
bpy.context.view_layer.update()

# rotate the control bone to align the larva's head
prev_control_mode = control_bone.rotation_mode
if prev_control_mode != 'XYZ':
    control_bone.rotation_mode = 'XYZ' 
            
a = math.radians(-22.1)  # Convert degrees to radians
b = control_bone.rotation_euler.copy()  # Copy the current rotation
new_euler = mathutils.Euler((b.x, b.y + a, b.z), 'XYZ')
control_bone.rotation_euler = new_euler
control_bone.rotation_mode = prev_control_mode 
bpy.context.view_layer.update()


# Apply the transform
armature = bpy.data.objects.get('Armature')
meshes = [obj.name for obj in bpy.data.objects if obj.type == 'MESH']

for mesh_name in meshes:
    mesh = bpy.data.objects.get(mesh_name)
    
    # Deselect all objects
    bpy.ops.object.select_all(action='DESELECT')
    
    # Select the mesh
    mesh.select_set(True)
    # Make the armature the active object
    bpy.context.view_layer.objects.active = mesh
    bpy.ops.object.mode_set(mode='OBJECT')
    armature_modifier = next((mod for mod in mesh.modifiers if mod.type == 'ARMATURE'), None)

    if armature_modifier:
        bpy.ops.object.modifier_add(type='ARMATURE')
        new_modifier = mesh.modifiers[-1]
        new_modifier.object = armature
        bpy.ops.object.modifier_apply(modifier=armature_modifier.name)
        
bpy.context.view_layer.objects.active = armature    
bpy.ops.object.mode_set(mode='POSE')
bpy.ops.pose.armature_apply()
bpy.ops.object.mode_set(mode='OBJECT')


# Return to Object Mode
bpy.ops.object.mode_set(mode='OBJECT')


# Convert the spine armature to a mesh so that is can be exported in the .obj
def convert_armature_to_mesh():
    '''
    Adpated from https://blenderartists.org/t/converting-armatures-to-mesh-objects/346603/12
    Consulted on 13/01/2025
    '''
    if bpy.context.object:
        bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.select_all(action='DESELECT')

    armature_meshname = "armature_mesh"
    armature = bpy.data.objects.get('Armature')

    def convertVector(v):
        return [v.x,v.y,v.z]

    #delete any previous attempts
    if bpy.data.objects.get(armature_meshname) is not None:
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects[armature_meshname]
        bpy.ops.object.delete(use_global=False)

    verts, edges, faces = [], [], []

    #select the armature and enter edit mode, and get all the bones. 
    bpy.context.view_layer.objects.active = armature
    bpy.ops.object.editmode_toggle()
    bpy.ops.armature.select_all(action='SELECT')
    bones = bpy.context.selected_bones

    #loop through the bones, putting all the coords in the data containers. 
    for bone in bones[:-1]:
        verts.append(convertVector(bone.head))
        verts.append(convertVector(bone.tail))
        vertId = len(verts)
        edges.append([vertId-2,vertId-1])


    bpy.ops.object.editmode_toggle()


    #make the object and add all the data.     
    mesh=bpy.data.meshes.new(armature_meshname)
    mesh.from_pydata(verts,edges,faces)
    newMesh=bpy.data.objects.new(armature_meshname,mesh)
    bpy.context.scene.collection.objects.link(newMesh)


    #connect the connected bones by removing doubles
    bpy.context.view_layer.objects.active = newMesh
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.remove_doubles()
    bpy.ops.object.editmode_toggle()
convert_armature_to_mesh()


print('done')