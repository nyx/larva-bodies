import bpy
import os

# create collection hierarchy

def create_collections():
    for collname in ['raw', 'simplified', 'cable']:
        if not(collname in bpy.data.collections):
            coll = bpy.data.collections.new(collname)
            bpy.context.scene.collection.children.link(coll)
        coll = bpy.data.collections[collname]
        for i in range(11):
            segment_collname = collname+'.'+str(i)
            if not(segment_collname in  bpy.data.collections):
                segment_coll = bpy.data.collections.new(segment_collname)
                coll.children.link(segment_coll)
            segment_coll =  bpy.data.collections[segment_collname]
            for side in ['g', 'd']:
                side_segment_collname = segment_collname + ' ' + side
                if not(side_segment_collname in bpy.data.collections):
                    side_segment_coll = bpy.data.collections.new(side_segment_collname)
                    segment_coll.children.link(side_segment_coll)
                side_segment_coll =  bpy.data.collections[segment_collname]
                
def import_and_sort_mesh(dir, fn):
    meshname, _ = os.path.splitext(fn)
            
    # build collection name
    if meshname.startswith('mesh'):
        collname = 'raw'
    elif meshname.startswith('simplified'):
        collname = 'simplified'
    elif meshname.startswith('cablemesh'):
        collname = 'cable'
    else:
        raise Error()     
    mesh_index = int(meshname.split('_')[-1])
    segment_index = mesh_index//100
    muscle_index = mesh_index%100
    side = 'd' if muscle_index < 50 else 'g'
    collname += '.'+str(segment_index)+' '+side
    
    # import mesh
    if not(meshname in bpy.data.objects):
        bpy.ops.wm.obj_import(filepath=os.path.join(dir, fn))
        
    # put mesh in proper collection
    mesh = bpy.data.objects[meshname]
    coll = bpy.data.collections[collname]
    if not(meshname in coll.objects):
        coll.objects.link(mesh)
    if meshname in bpy.context.scene.collection.objects:
        bpy.context.scene.collection.objects.unlink(mesh)
        
def import_and_sort_all_meshes():       
    root = "/mnt/hecatonchire/Current-Software-LAB/Nyx/larva-bodies/assets/meshes"
    for dir in ["muscle_meshes", "simplified_muscle_meshes", "corrected_cable_muscle_meshes/obj"]:
        dir = os.path.join(root, dir)
        for filename in os.listdir(dir):
            import_and_sort_mesh(dir, filename)
            
if __name__ == '__main__':
    create_collections()
    import_and_sort_all_meshes()
       
        
        
