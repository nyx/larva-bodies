import bpy
import os

rootcollname = 'checker'
if not(rootcollname in bpy.data.collections):
    rootcoll = bpy.data.collections.new(rootcollname)
    bpy.context.scene.collection.children.link(rootcoll)
rootcoll = bpy.data.collections[rootcollname]

root = "/mnt/hecatonchire/Current-Software-LAB/Nyx/assets/meshes"
for dir in ["muscle_meshes", "raw_cable_muscle_meshes/obj"]:
    dir = os.path.join(root, dir)
    for filename in os.listdir(dir):
        meshname, _ = os.path.splitext(filename)
        mesh_index = int(meshname.split('_')[-1])
        segment_index = mesh_index//100
        muscle_index = mesh_index%100
        
        # build collection name
        collname = f'muscle_{muscle_index}'
        if not(collname in bpy.data.collections):
            coll = bpy.data.collections.new(collname)
            rootcoll.children.link(coll)
        
        if meshname.startswith('mesh'):
            subcollname = collname+'.raw'
        elif meshname.startswith('cablemesh'):
            subcollname = collname+'.cable'
        else:
            raise Error()     
        if not(subcollname in  bpy.data.collections):
            coll = bpy.data.collections[collname]
            subcoll = bpy.data.collections.new(subcollname)
            coll.children.link(subcoll)
        subcoll =  bpy.data.collections[subcollname]
        
        
        
        # import mesh
        if not(meshname in bpy.data.objects):
            bpy.ops.wm.obj_import(filepath=os.path.join(dir, filename))
            
        # put mesh in proper collection
        mesh = bpy.data.objects[meshname]

        if not(meshname in subcoll.objects):
            subcoll.objects.link(mesh)
        if meshname in bpy.context.scene.collection.objects:
            bpy.context.scene.collection.objects.unlink(mesh)

def get_muscle_index_from_collection_name(collname):
    return int(collname[7:])

for c in sorted(rootcoll.children, key=lambda c: get_muscle_index_from_collection_name(c.name)):
    rootcoll.children.unlink(c)
    rootcoll.children.link(c)

        
        
