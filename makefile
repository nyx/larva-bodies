deploy : cluster/larvabodies.sif .git
	ssh aleblanc@maestro.pasteur.fr -t 'cd workspace/larva-bodies;git pull'

cluster/larvabodies.sif : cluster/larvabodies.def ${SOFA_ROOT}/bin/runSofa
	apptainer build --force cluster/larvabodies.sif cluster/larvabodies.def
	git add cluster/larvabodies.sif
	git commit -m "Newly built SIF image"
	git push