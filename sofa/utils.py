def add_required_plugins(rootNode):
    rootNode.addObject('RequiredPlugin', pluginName=[
                            "Sofa.Component.AnimationLoop",  # Needed to use components FreeMotionAnimationLoop
                            "Sofa.Component.Visual",  # Needed to use components VisualStyle
                            "Sofa.Component.Constraint.Lagrangian.Correction", # Needed to use components LinearSolverConstraintCorrection
                            "Sofa.Component.Constraint.Lagrangian.Solver",  # Needed to use components GenericConstraintSolver
                            "Sofa.Component.Constraint.Projective",  # Needed to use components FixedConstraint
                            "Sofa.Component.Engine.Select",  # Needed to use components BoxROI
                            "Sofa.Component.IO.Mesh",  # Needed to use components MeshVTKLoader
                            "Sofa.Component.LinearSolver.Direct",  # Needed to use components SparseLDLSolver
                            "Sofa.Component.LinearSolver.Iterative",  # Needed to use components ShewchukPCGLinearSolver
                            "Sofa.Component.Mass",  # Needed to use components UniformMass
                            "Sofa.Component.ODESolver.Backward",  # Needed to use components EulerImplicitSolver
                            "Sofa.Component.SolidMechanics.FEM.Elastic",  # Needed to use components TetrahedronFEMForceField
                            "Sofa.Component.SolidMechanics.Spring",  # Needed to use components TetrahedronFEMForceField
                            "Sofa.Component.Topology.Container.Constant",  # Needed to use components MeshTopology
                            "Sofa.Component.Visual",  # Needed to use components VisualStyle
                            "Sofa.Component.Mapping.Linear", # Needed to use components BarycentricMapping  
                            "Sofa.Component.StateContainer", # Needed to use components MechanicalObject
                            "Sofa.Component.Setting", # Needed to use components BackgroundSetting  
                            "Sofa.Component.Topology.Container.Dynamic", # Needed to use components TetrahedronSetGeometryAlgorithms,TetrahedronSetTopologyContainer  
                            "Sofa.GL.Component.Rendering3D", # Needed to use components OglModel,OglSceneFrame
                            "Sofa.Component.Collision.Geometry", # Needed to use components [LineCollisionModel,PointCollisionModel,TriangleCollisionModel]  
                            "Sofa.Component.Collision.Response.Contact", # Needed to use components [CollisionResponse]  
                            "Sofa.Component.Mapping.NonLinear", # Needed to use components [RigidMapping]  
                            "Sofa.Component.Topology.Mapping", # Needed to use components [Tetra2TriangleTopologicalMapping] 
                            "CGALPlugin",
                            "Sofa.Component.Collision.Detection.Algorithm",
                            "Sofa.Component.Collision.Detection.Intersection",
                            "MultiThreading", # Modified by Paul Baksic to improve performance
                            "SofaPython3",
                            "SoftRobots",
                        ])
    
def add_collision_pipeline(rootNode, cfg):
    s = cfg.larva_length
    rootNode.addObject('CollisionPipeline')
    rootNode.addObject('ParallelBruteForceBroadPhase') # Modified by Paul Baksic to improve performance
    rootNode.addObject('ParallelBVHNarrowPhase') # Modified by Paul Baksic to improve performance
    rootNode.addObject('LocalMinDistance',
                        alarmDistance=0.01*s, contactDistance=0.001*s,
                        angleCone=0.5*s)

def add_solver(bodyMechanics, cfg):
    kwargs = dict()
    if 'stiffness' in cfg.rayleighType:
        kwargs['rayleighStiffness'] = cfg.rayleighStiffness
    if 'mass' in cfg.rayleighType:
        kwargs['rayleighMass'] = cfg.rayleighMass
    bodyMechanics.addObject('EulerImplicitSolver', **kwargs)

def get_git_revision_hash() -> str:
    import subprocess
    import pathlib
    return subprocess.check_output(['git', 'rev-parse', 'HEAD'], cwd=pathlib.Path(__file__).parent).decode('ascii').strip()