from stlib3.physics.rigid import Floor
from sofa.muscles import load_muscles, MuscleController
from sofa.recording import RecordingController
from sofa.config import PROJECT_DIR, default_cfg
import sofa.utils as utils
from trajectories.trajectory_builder import manualCrawlTrajectory, manualRollTrajectory

# Units : mm, s and kg


def createScene(rootNode, cfg=default_cfg):
    s = cfg.larva_length # s for scale
    # larva parameters
    trajectory = cfg.trajectory()

    rootNode.findData('dt').value = cfg.dt
    rootNode.findData('gravity').value = [0, 0, cfg.gravity if cfg.use_gravity else 0]

    # rootNode.addObject('VisualStyle', displayFlags='showCollision showForceFields showInteractionForceFields'
                                    #  +' hideCollisionModels hideBoundingCollisionModels hideVisualModels showWireframe')
    
    # Required plugins
    utils.add_required_plugins(rootNode)
    
    rootNode.addObject('BackgroundSetting', color=[0, 0.168627, 0.211765, 1]) # set background color
    rootNode.addObject('OglSceneFrame', style="Arrows", alignment="TopRight") # add a triedron to help with orientation

    # setup the animation loop
    rootNode.addObject('FreeMotionAnimationLoop', parallelODESolving="true", parallelCollisionDetectionAndFreeMotion="true") # Modified by Paul Baksic to improve performance
    rootNode.addObject('GenericConstraintSolver', maxIterations=5000, tolerance=1e-12)

    # add a collision pipeline
    utils.add_collision_pipeline(rootNode, cfg)
    
    rootNode.addObject('CollisionResponse', response='FrictionContactConstraint', responseParams=f"mu={cfg.frictionMu}&drag={cfg.frictionDrag}")

    # Add a floor
    if cfg.use_gravity:
        Floor(rootNode, rotation=[90, 0, 0], translation=[0,0,-.1*s], uniformScale=.02*s, isAStaticObject=True)

    # create the larva object

    larvaBody = rootNode.addChild('larvaBody')

    # load the detailed mesh
    larvaOBJ = str(PROJECT_DIR/'assets'/'meshes'/'cuticle_meshes'/'cuticle_simplified_cleaned_straightened.obj')
    larvaBody.addObject('MeshOBJLoader', name='larvaMesh', filename=larvaOBJ, rotation="90 0 0", scale=s)
    
    # create the overall body mechanics
    bodyMechanics = larvaBody.addChild('bodyMechanics')
    
    # use CGAL to create a simplified tetrahedral mesh of the larva
    # parameters to be adjusted TODO
    # Compare number of DOFs in the mesh to number of constraints due to muscles !
    bodyMechanics.addObject('MeshGenerationFromPolyhedron', name='gen', template='Vec3d', inputPoints='@../larvaMesh.position', inputTriangles='@../larvaMesh.triangles', drawTetras='0',
                       cellSize=f'{s*cfg.cellSize}', facetAngle="30", facetSize=f'{s*.05}', cellRatio="2",   #Convergence problem if lower than 2
                       facetApproximation=f'{s*.01}')
    
    # Define resolution method
    utils.add_solver(bodyMechanics, cfg) # define numerical scheme

    bodyMechanics.addObject('SparseLDLSolver', template="CompressedRowSparseMatrixMat3x3d") # define solve method
    
    # Create the state from the coarse mesh
    bodyMechanics.addObject('MechanicalObject', template='Vec3d', name="dofs", position="@gen.outputPoints")
    bodyMechanics.addObject('TetrahedronSetTopologyContainer', name='topo', tetrahedra='@gen.outputTetras')
    bodyMechanics.addObject('TetrahedronSetGeometryAlgorithms', template="Vec3d", name="GeomAlgo", drawTetrahedra="0", drawScaleTetrahedra="0.8")
    
    # Set the mechanical parameters and force fields
    bodyMechanics.addObject('MeshMatrixMass', totalMass=cfg.totalLarvaMass) # add mass
    bodyMechanics.addObject('ParallelTetrahedronFEMForceField', youngModulus=cfg.youngModulus, poissonRatio=cfg.poissonRatio, method='large') # add elasticity

    
    # Fix the tail of the larva, so that it is anchored and we can deform the larva by pulling on it
    if cfg.fix_tail:
        x, y, z = -.5*s,-.1*s,-.1*s
        dx, dy, dz = .12*s,.2*s,.2*s
        bodyMechanics.addObject('BoxROI', name="boxROI", box=[x,y,z,x+dx,y+dy,z+dz], drawBoxes=True) # create an anchor or selection zone (red skeleton)
        bodyMechanics.addObject('FixedConstraint', indices="@boxROI.indices") # add the constraint
    if cfg.fix_head:
        x, y, z = .4*s,-.1*s,-.1*s
        dx, dy, dz = .12*s,.2*s,.2*s    
        bodyMechanics.addObject('BoxROI', name="boxROI2", box=[x,y,z,x+dx,y+dy,z+dz], drawBoxes=True) # create an anchor or selection zone (red skeleton)
        bodyMechanics.addObject('FixedConstraint', indices="@boxROI2.indices") # add the constraint
    if cfg.pull_head:
       bodyMechanics.addObject('ConstantForceField', indices=[147], forces=[[5e-6,0,0]]) # add constant force

    bodyMechanics.addObject('LinearSolverConstraintCorrection') # add the constraint solver
    
    if cfg.use_cuticle:
        cuticleMechanics = bodyMechanics.addChild('cuticleMechanics')
        cuticleMechanics.addObject('TriangleSetTopologyContainer', name="Container")
        cuticleMechanics.addObject('TriangleSetTopologyModifier')
        cuticleMechanics.addObject('Tetra2TriangleTopologicalMapping', input="@../topo", output="@Container")

        cuticleMechanics.addObject('TriangleBendingSprings', stiffness=100, damping=5) # add elasticity
        cuticleMechanics.addObject('TriangleFEMForceField', stiffness=100, damping=5) # add elasticity

    if cfg.use_visuals:
        # Set a visual model
        visualModel = larvaBody.addChild('visualModel')
        visualModel.addObject('OglModel', name='visualMesh', src='@../larvaMesh')
        # Align the visual model on the coarse mechanical mesh
        visualModel.addObject('BarycentricMapping', input='@../bodyMechanics/dofs', output='@visualMesh')

    # Collision model
    collision = bodyMechanics.addChild('larvaCollisionModel') # collision model
    collision.addObject('TriangleSetTopologyContainer', name="Container")
    collision.addObject('TriangleSetTopologyModifier')
    collision.addObject('Tetra2TriangleTopologicalMapping', input="@../topo", output="@Container")

    collision.addObject('TriangleCollisionModel', selfCollision=True) # 3 types of collision
    collision.addObject('LineCollisionModel', selfCollision=True)
    collision.addObject('PointCollisionModel', selfCollision=True)

    # build a muscle model
    if cfg.use_muscles:
        musclesNode = rootNode.addChild('Muscles')
        muscles = load_muscles(cfg)
        for muscle in muscles:
            muscle.build(musclesNode)
        rootNode.addObject(MuscleController(name='muscleController', 
                                            node=rootNode, 
                                            traj_params=trajectory, 
                                            maxForce=cfg.Fmax_per_muscle, 
                                            tau=default_cfg.muscle_tau, 
                                            hill_w=default_cfg.hill_w, 
                                            start_delay=0))

    if cfg.record:
        rootNode.addObject(RecordingController(name='recordingController', node=rootNode, rate=cfg.recordRate, dest=cfg.recordDirectory/cfg.recordSubdirectory/cfg.recordFilename))
    return rootNode

