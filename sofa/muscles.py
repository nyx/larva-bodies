import itertools
from dataclasses import dataclass
from sofa.config import *
import numpy as np
import Sofa
import matplotlib

@dataclass
class Muscle:
    name: str
    meshfile: str
    cables: list

    def build(self, rootNode):
        muscleNode = rootNode.addChild(self.name)
        muscleNode.addObject('MechanicalObject', name='cabledofs', position=self.cables[0][[0,-1]].tolist()[0:])
        cable = muscleNode.addObject('CableConstraint', name='cableconstraint', indices=list(range(len(self.cables[0][[0,-1]]))), hasPullPoint=False, valueType='force')
        muscleNode.addObject('BarycentricMapping', name='cablemapping', input='@../../larvaBody/bodyMechanics/dofs', output=f'@cabledofs')
        cable.drawPoints.value = 0
        cable.drawPullPoint.value = 0

def load_muscles(cfg):
    R = np.array([[1, 0,  0],
                  [0, 0, -1],
                  [0, 1,  0]])
    asset_dir = f'{PROJECT_DIR}/assets/meshes/corrected_cable_muscle_meshes/npy'
    files = [s for s in os.listdir(asset_dir) if s.startswith('cable')]
    muscles = []
    for filename in files:
        index = int(filename.split('.')[0].split('_')[-1])
        if index < 300 or index > 1000:
            continue
        name = 'muscle_'+str(index)
        meshfile = 'simplified_'+filename[5:-4]+'.obj'

        cables = [cfg.larva_length*np.load(os.path.join(asset_dir, filename))@R.transpose()]
        if np.isnan(cables[0]).any():
            continue
        muscles.append(Muscle(name, meshfile, cables))
    muscles.sort(key=lambda x:x.name)
    return muscles

def v_multiplier(v, v_max=-5, K=5, N=1.5):
    return 1.
    if v >= 0:
        return (v_max-v)/(v_max+K*v)
    return N + (N-1)*(v_max+v)/(7.56*K*v-v_max)

def l_multiplier(l, l_opt, width=.4):
    return np.exp(np.log(0.05)*np.abs((l-l_opt)/(width*l_opt))**3)

class MuscleController(Sofa.Core.Controller):
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.node = kw["node"]
        self.traj_params = kw["traj_params"]
        self.traj = self.traj_params.create_trajectory()
        self.activation = {k:0. for k in self.traj(0)}
        self.start_delay = kw.get('start_delay', 0.03)
        self.maxForce = kw.get('maxForce', 6.7/28)
        self.time = 0
        self.cmap = matplotlib.colormaps['magma']
        self.rest_length = self.init_rest_length()
        self.tau = kw.get('tau',.05)
        self.hill_w = kw.get('hill_w',.4)
        self.trajectory_finished = False
        self.update_colors()

        print('\033[92m[INFO]\033[0m    \033[94m[MuscleController]\033[0m Controller initialized')

    def init_rest_length(self):
        rest_length = {}
        for name in self.activation:
            muscleNode = self.node.getChild('Muscles').getChild(name)
            if muscleNode is None:
                print(f'\033[93m[WARNING]\033[0m \033[94m[MuscleController]\033[0m Node {name} not found in graph')
                continue
            # get length and velocity of cable
            extremities = muscleNode.getObject('cabledofs').position.value
            direction = extremities[0]-extremities[1]
            length = np.linalg.norm(direction)
            rest_length[name] = length
        return rest_length

    def onAnimateBeginEvent(self, e):
        self.dt = e['dt']
        self.time += self.dt

        if self.time < self.start_delay:
            return
        
        t = self.time - self.start_delay
        self.phase = t%self.traj_params.duration
        if t > self.traj_params.duration:
            self.trajectory_finished = True
        self.update_activations()
        self.update_forces()
        self.update_colors()

    def update_colors(self):
        for name in self.activation:
            muscleNode = self.node.getChild('Muscles').getChild(name)
            if muscleNode is None:
                continue
            cable = muscleNode.getObject('cableconstraint')

            color = self.cmap(self.activation[name])
            cable.color.value = list(color)

    def update_activations(self):
        new_control = self.traj(self.phase)
        w = 1/(1+self.dt/self.tau)
        self.activation = {name:w*activation + (1-w)*new_control[name] for name, activation in self.activation.items()}

    def update_forces(self):
        for name in self.activation:
            muscleNode = self.node.getChild('Muscles').getChild(name)
            if muscleNode is None:
                # print(f'Node {name} not found in graph')
                continue

            # get length and velocity of cable
            extremities = muscleNode.getObject('cabledofs')
            extremities_position = extremities.position.value
            extremities_velocity = extremities.velocity.value
            direction = extremities_position[0]-extremities_position[1]
            length = np.linalg.norm(direction)
            direction = direction/length
            velocity = np.dot(extremities_velocity[0]-extremities_velocity[1], direction)

            force = self.maxForce*self.activation[name]*l_multiplier(length, self.rest_length[name], self.hill_w)*v_multiplier(velocity)
            cable = muscleNode.getObject('cableconstraint')
            cable.value.value = [force]