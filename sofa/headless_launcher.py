import Sofa
from sofa.scene import createScene
from sofa.config import default_cfg, dump_config_file
from tqdm import trange
import numpy as np
import sofa.utils as utils
import os

def create_history_subfolder(cfg):
    path = cfg.recordDirectory/cfg.recordSubdirectory
    os.makedirs(path, exist_ok=True)
    with open(path/'commit_hash.txt', 'w') as f:
        f.write(utils.get_git_revision_hash())
    dump_config_file(path/'config.json', cfg)


class StabilityChecker:
    def __init__(self, root):
        self.root = root
        self.topology = self.root.larvaBody.bodyMechanics.topo.edges.value
        self.dofs = self.root.larvaBody.bodyMechanics.dofs.position
        self.rest_lengths = self.compute_lengths()
        self.threshold = np.inf

    def compute_lengths(self):
        dofs = self.dofs.value
        return np.linalg.norm(dofs[self.topology[:,0]]-dofs[self.topology[:,1]], axis=1)
    
    def is_stable(self):
        lengths = self.compute_lengths()
        energy = np.sum((lengths-self.rest_lengths)**2)
        print(energy)
        # self.get_energy()
        return energy < self.threshold
    
    # def get_energy(self):
    #     print('KE : ', self.root.larvaBody.bodyMechanics.MeshMatrixMass.getKineticEnergy())
    #     print('EE : ', self.root.larvaBody.bodyMechanics.ParallelTetrahedronFEMForceField.getPotentialEnergy())




def check_stability(state, previous_state):
    return np.isclose(state, previous_state, atol=1e-10, rtol=1e-3)

def launch_scene(cfg=default_cfg):
    # Call the SOFA function to create the root node
    root = Sofa.Core.Node("root")
    create_history_subfolder(cfg)
    # Call the createScene function, as runSofa does
    createScene(root, cfg=cfg)

    # Once defined, initialization of the scene graph
    Sofa.Simulation.init(root)
    stability_checker = StabilityChecker(root)

    # Run as many simulation steps (here 10 steps are computed)
    for step in trange(cfg.steps):
        Sofa.Simulation.animate(root, root.dt.value)
        state = root.larvaBody.bodyMechanics.dofs.position.value
        if stability_checker.is_stable():
            continue
        else:
            print("Simulation diverged !")
            break



# Function used only if this script is called from a python environment
if __name__ == '__main__':
    default_cfg.recordSubdirectory = 'test'
    launch_scene()
