from collections import defaultdict
import itertools
import Sofa
import numpy as np
from time import time, sleep

class RecordingController(Sofa.Core.Controller):
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.node = kw["node"]
        self.rate = kw["rate"]
        self.dest = kw['dest']
        self.interval = 1/self.rate
        self.time = 0
        self.records = defaultdict(list)

    def onAnimateBeginEvent(self, e):
        self.dt = e['dt']
        self.time += self.dt
        if self.time%self.interval < self.dt:
            self.record()
            self.commit()

    def record(self):
        self.records['times'].append(self.time)
        self.record_muscle()
        self.record_mesh_positions()
        if not('topology' in self.records):
            self.record_topology()

    def record_muscle(self):
        for (segment, muscle, side) in itertools.product(range(3,10), range(1,31), [0,50]):
            index = segment*100+side+muscle
            name = f'muscle_{index}'

            muscleNode = self.node.getChild('Muscles').getChild(name)
            if muscleNode is None:
                continue

            cable = muscleNode.getObject('cableconstraint')
            self.records[name+'_activity'].append(cable.value.value[0])
            self.records[name+'_color'].append(cable.color.value.copy())

    def record_mesh_positions(self):
        body_dofs = self.node.getChild('larvaBody').getChild('bodyMechanics').getObject('dofs')
        self.records['body_positions'].append(body_dofs.position.value.copy())
    
    def record_topology(self):
        topo = self.node.larvaBody.bodyMechanics.topo
        self.records['edges'] = topo.edges.value
        self.records['triangles'] = topo.triangles.value
        self.records['tets'] = topo.tetrahedra.value
        self.records['topology'] = None

    def commit(self):
        muscle_activities = []
        muscle_colors = []
        muscle_names = []
        for (segment, muscle, side) in itertools.product(range(3,10), range(1,31), [0,50]):
            index = segment*100+side+muscle
            name = f'muscle_{index}'
            muscleNode = self.node.getChild('Muscles').getChild(name)
            if muscleNode is None:
                continue
            muscle_activities.append(self.records[name+'_activity'])
            muscle_colors.append(np.stack(self.records[name+'_color']))
            muscle_names.append(name)
        muscle_activities = np.array(muscle_activities)
        muscle_colors = np.array(muscle_colors)
        muscle_names = np.array(muscle_names, dtype=str)
        edges = np.array(self.records['edges'], dtype=int)
        triangles = np.array(self.records['triangles'], dtype=int)
        tets = np.array(self.records['tets'], dtype=int)
        body_positions = np.stack(self.records['body_positions'])
        np.savez(self.dest, times=self.records['times'],
                            muscle_activities=muscle_activities,
                            muscle_names=muscle_names,
                            muscle_colors=muscle_colors,
                            body_positions=body_positions,
                            edges=edges,
                            triangles=triangles,
                            tets=tets)


class ReplayController(Sofa.Core.Controller):
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.node = kw["node"]
        self.records = kw["records"]
        self.stepsize = kw.get("stepsize", 1)
        self.step=0
        self.len =len(self.records['body_positions'])
        self.update_colors()

        print('[INFO]    [MuscleController] Controller initialized')


    def onAnimateBeginEvent(self, e):
        self.step_start_time = time()
        body_dofs = self.node.getChild('larvaBody').getChild('bodyMechanics').getObject('dofs')
        body_dofs.position = self.records['body_positions'][self.step]

        self.update_colors()
        self.step += self.stepsize
        self.step = self.step%self.len

    # def onAnimateEndEvent(self, e):
        # elapsed_time = time() - self.step_start_time
        # delay = max(1/24-elapsed_time,0)
        # print(elapsed_time)
        # sleep(delay)

    def update_colors(self):
        colors = self.records['muscle_colors'][:,self.step]
        for idx, name in enumerate(self.records['muscle_names']):

            muscleNode = self.node.getChild('Muscles').getChild(name)
            if muscleNode is None:
                continue
            cable = muscleNode.getObject('cableconstraint')
            color = colors[idx]
            cable.color.value = color