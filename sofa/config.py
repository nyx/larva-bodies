import os
from pathlib import Path
import json
from dataclasses import dataclass

PROJECT_DIR = Path(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Units : m, s and kg -> N, Pa

@dataclass
class Config:
    traj_type:str = os.environ.get('TRAJ_TYPE', 'CSHAPE')

    cellSize:float = .02
    frictionDrag:float = 0 # N*s*m^-1
    frictionMu:float = 1 # N*s*m^-1

    Fmax:float = 6.7e-3 # N
    Fmax_per_muscle:float = 3*Fmax/28 # N
    totalLarvaMass:float = 15e-6# kg

    gravity:float = -9.81 #m*s^-2

    # k_whole:float = 8.6 # mN/mm
    # c_whole:float = 2.6e2 # mN.s/mm
    larva_length:float = 3.53e-3 # m    
    # cuticle_thickness:float = 1e-2 # mm

    dt:float = 1e-3 # s
    steps:int = 10000

    rayleighType:str = "stiffness" # stifness, mass or stiffness+mass
    rayleighMass:float = 1e-2
    rayleighStiffness:float = 1e-2

    fix_tail:bool = False
    fix_head:bool = False
    pull_head:bool = False
    use_cuticle:bool = False
    
    use_gravity:bool = True
    use_muscles:bool = True
    use_visuals:bool = False
    record:bool = True

    youngModulus:float = 1e8 # Pa
    poissonRatio:float = .498

    muscle_tau:float = .05 # s
    hill_w:float = .4

    recordRate:float = 1e3
    recordFilename:str = 'records.npz'
    recordDirectory:str = Path(os.environ.get('LARVABODIES_HISTORY', PROJECT_DIR/'history'))
    recordSubdirectory:str = Path('temp')

    def trajectory(self):
        import trajectories.trajectory_builder as traj
        match self.traj_type:
            case 'ROLL':
                return traj.manualRollTrajectory
            case 'CRAWL':
                return traj.manualCrawlTrajectory
            case 'CSHAPE':
                return traj.manualCShapeTrajectory
            case _:
                raise ValueError("Inappropriate traj type")
    
    @classmethod
    def float_args(cls):
        args = []
        for argname in vars(cls):
            if argname.startswith('__'):
                continue
            elif isinstance(getattr(cls, argname), float):
                args.append(argname)
        return args

default_cfg = Config()

def load_config_file(filename):
    with open(filename, 'r') as f:
        config_dict = json.load(f)
    for k, v in config_dict.items():
        if 'directory' in k.lower():
            config_dict[k] = Path(v)

    return Config(**config_dict)

def dump_config_file(filename, cfg):
    cfg_dict = dict(vars(cfg))
    for k, v in cfg_dict.items():
        if isinstance(v, Path):
            cfg_dict[k] = str(v)
    with open(filename, 'w') as f:
        json.dump(cfg_dict, f, indent=4)

if __name__ == '__main__':
    dump_config_file('temp.json', default_cfg)
    cfg = load_config_file('temp.json')
    assert(cfg == default_cfg)    