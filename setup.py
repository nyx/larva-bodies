from distutils.core import setup

setup(name='LarvaBodies',
      version='0.0',
      description='Larva simulator',
      author='Alexandre Blanc',
      author_email='aleblanc@pasteur.fr',
      packages=['cluster', 'sofa', 'trajectories', 'analysis'],
     )