# Larva bodies

## Contents

* `assets/larva_ct_scan.tif` : CT-scan of the larva
* `assets/muscle_labels_13022024.tif` : muscle labels

----------
* `assets/meshes/cuticle_meshes` : meshes of the cuticle of the larva
* `assets/meshes/muscle_meshes` : raw muscles extracted from the muscle labels with marching cubes
* `assets/meshes/simplified_muscle_meshes` : rectangles fitted to muscles using the LDDM method
* `assets/meshes/raw_cable_muscle_meshes` : cables extracted from the simplified muscle meshes, saved as .obj and .npy files
* `assets/meshes/unrolled_muscle_meshes` : raw muscle meshes projected in polar coordinates, with a cut on the top of the larva or the bottom of the larva
----------

* `meshing/mesh.py` : code for most of the mesh generation
* `meshing/simplify_muscle_meshes` : code for rectangle and cable extraction
* `meshing/blender_scripts` : scripts to build the blender scenes used to visualize the muscles and correct the cables

Run build_checker.py to create the blender larva_checker.blend file. Using this file, you can manually correct the positions of the extracted cables.
Save the result as larva_checker_final.blend.
Copy this file and rename it larva_straightener.blend. This file will be used to straighten the larva in its rest position.

Then, run the straighten_larva.py script. This will straighten the larva and create a mesh corresponding to the approximate midline of the larva.
Export the scene as an .OBJ file, larva_straightener.obj. You can then parse this .OBJ file using parse_larva_straightener.py to extract the meshes.

This will populate the folder assets/corrected_cable_muscle_meshes, create the file cuticle_meshes/cuticle_simplified_cleaned_straightened.obj and the populate the folder assets/larva_midline.
--------
* `sofa` : simulation code
--------
* `cluster` : tools to launch a simulation on the cluster.
* `cluster/larvabodies.sif` : apptainer image containing SOFA
* `cluster/launch_job.sh` : entry point for jobs that explore parameter grids
example :

```
larva-bodies/cluster/launch_job.sh --traj_type CSHAPE --youngModulus 1e7 1e9 10 log --Fmax 1e-3 1e-2 10 log --rayleighStiffness 1e-4 1e0 10 log
```
Available trajectory types are CSHAPE, ROLL and CRAWL. Any floating point parameter in the configuration of the simulation can be passed as a parameter in the form of a range. The syntax is `--paramName start stop steps [lin|log]`

The trajectories are recorded in `history`, in a folder named after the time and date of the job submission. Each combination of the parameters is stored in its own folder, named after the parameters, which contains the configuration as a `json` file, the `git` commit hash, and the records of the trajectory as a `.npz` file.
---------
