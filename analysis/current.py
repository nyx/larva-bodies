import numpy as np

# Current based distance metric

def get_arclength(outline):
    '''outline : N x D array of N sampling points in D dimensions'''    
    return np.pad(np.cumsum(np.linalg.norm(np.diff(outline, axis=0), axis=1)), (1,0))
def get_tangent_vectors(outline):
    '''outline : N x D array of N sampling points in D dimensions'''    
    return np.diff(np.concatenate([outline[-1:,:], outline]), axis=0)
def get_segment_length(outline):
    '''outline : N x D array of N sampling points in D dimensions'''    
    v = get_tangent_vectors(outline)
    return np.linalg.norm(v, axis=1, keepdims=True)
def get_normalized_tangent_vector(outline):
    '''outline : N x D array of N sampling points in D dimensions'''    
    v = get_tangent_vectors(outline)
    l = get_segment_length(outline)
    # print(l)
    return v/l
def get_midpoints(outline):
    '''outline : N x D array of N sampling points in D dimensions'''    
    return .5*(np.concatenate([outline[-1:,:], outline[:-1,:]])+outline)

def k(x,y,sigma=1.0):
    return np.exp(-np.sum((x-y)**2/(2*sigma**2), axis=0))

def current_dot_product(outline1, outline2, sigma=1.):
    '''outline : N x D array of N sampling points in D dimensions'''
    # remove duplicate points
    dl1 = get_segment_length(outline1).flatten()
    dl2 = get_segment_length(outline2).flatten()
    outline1 = outline1[dl1 > 1e-15,:]
    outline2 = outline2[dl2 > 1e-15,:]

    dl1 = get_segment_length(outline1)
    dl2 = get_segment_length(outline2)
    x = get_midpoints(outline1)
    y = get_midpoints(outline2)

    K = k(x.transpose().reshape(x.shape[1], x.shape[0], 1),
        y.transpose().reshape(y.shape[1], 1, y.shape[0]),
        sigma)
    t1 = get_normalized_tangent_vector(outline1)
    t2 = get_normalized_tangent_vector(outline2)
    dot = np.sum(t1.reshape(t1.shape[0], 1, t1.shape[1])
                *t2.reshape(1, t2.shape[0], t2.shape[1]),
                axis=-1)

    integral = np.sum(K*dot*dl1*dl2.transpose())

    return integral

def current_based_distance_squared(outline1, outline2, sigma=1.0):
    '''outline : N x D array of N sampling points in D dimensions'''
    return current_dot_product(outline1, outline1, sigma) - 2*current_dot_product(outline1, outline2, sigma) + current_dot_product(outline2, outline2, sigma)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    def test_current_helpers():
        th = np.linspace(0,2*np.pi,20,endpoint=False)
        x, y = np.cos(th), np.sin(th)
        outline = np.stack([x, y], axis=-1)

        plt.figure()
        plt.plot(x, y, 'o-')
        c = get_midpoints(outline)
        v = get_normalized_tangent_vector(outline)
        plt.quiver(c[:,0], c[:,1], v[:,0], v[:,1])
        plt.axis('equal')
        plt.show()
    test_current_helpers()

    def test_current_distance():
        N = 20
        th = np.linspace(0,2*np.pi,N,endpoint=False)
        x, y = np.cos(th), np.sin(th)
        outline = np.stack([x, y], axis=-1)
    
        th = np.linspace(0,2*np.pi,N+5,endpoint=False)
        x, y = np.cos(th), np.sin(th)
        outline2_base = np.stack([x, y], axis=-1)

        distances = []
        offsets = np.linspace(1,0,10)
        for offset in offsets:
            outline2 = outline2_base.copy()
            outline2[:,0] = outline2[:,0] + offset
            distances.append(current_based_distance_squared(outline, outline2, sigma=0.5))
        plt.plot(offsets, distances)
        plt.show()
    test_current_distance()
