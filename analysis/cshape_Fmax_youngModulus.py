from analysis.outline import compute_outline, plot_outline
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from dataclasses import dataclass
@dataclass
class Mesh:
    x:list[float]
    y:list[float]
    z:list[float]
    i:list[int]
    j:list[int]
    k:list[int]
    
def check_parameters(root):
    root = Path(root)
    body_positions = {}
    for fn in root.glob('*.npz'):
        data = np.load(fn)
        tokens = fn.stem.split('_')
        youngModulus = float(tokens[-1])
        Fmax = float(tokens[-3])

    return body_positions

def get_last_body_positions(root):
    root = Path(root)
    body_positions = {}
    for fn in root.glob('*.npz'):
        data = np.load(fn)
        tokens = fn.stem.split('_')
        youngModulus = float(tokens[-1])
        Fmax = float(tokens[-3])
        edges = data['edges']
        tetras = data['tets']
        body_positions[(youngModulus, Fmax)] = (data['body_positions'][-1], edges, tetras)

    return body_positions

def plot_outline_grid(body_positions):
    fig, axs = plt.subplots(10,10,figsize=(20,20))
    fmax = sorted(set([fmax for _,fmax in body_positions]))
    yms = sorted(set([ym for ym,_ in body_positions]))
    for ym, ax in zip(yms, axs[0]):
        ax.set_title(f'{ym:.2e}')

    for fm, axs_fm in zip(fmax, axs):
        axs_fm[0].set_ylabel(f'{fm:.3f}')
        for ym, ax in zip(yms, axs_fm):
            data = body_positions[(ym, fm)]
            outline_nodes, outline_edges = compute_outline(*data)
            plot_outline(ax, outline_nodes, outline_edges, c='k')
            # ax.set_title(f'{ym:.2e} {fm:.3f}')
            ax.axis('equal')
            # ax.axis('off')
            ax.set_xticks([])
            ax.set_yticks([])
    fig.suptitle('Young modulus')
    fig.supylabel('Fmax')
    plt.savefig('/home/alexandre/workspace/larva-bodies/history/temp/batch/fig.jpg')
    plt.show()



if __name__ == '__main__':
    body_positions = get_last_body_positions('/home/alexandre/workspace/larva-bodies/history/temp/batch')

    plot_outline_grid(body_positions)