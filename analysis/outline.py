from sofa.config import default_cfg
import numpy as np
import matplotlib.pyplot as plt
import itertools
from scipy.spatial import ConvexHull



def compute_boundary(vertices, edges, tetras):
    tris = list(itertools.chain(*([tuple(sorted(p)) for p in itertools.combinations(tet, r=3)] for tet in tetras)))
    outertris = set()
    for tri in tris:
        assert(tri[0]<tri[1] and tri[1]<tri[2])
        if tri in outertris:
            outertris.remove(tri)
        else:
            outertris.add(tri)
    outerpoints_set = set(p for tri in outertris for p in tri)
    outerpoints = list(sorted(outerpoints_set))
    mapping = {all_id:outer_id for outer_id, all_id in enumerate(outerpoints)}
    outer_edges = [[mapping[e1], mapping[e2]] for e1, e2 in edges if e1 in outerpoints_set and e2 in outerpoints_set]
    outer_vertices = vertices[outerpoints,:2]

    return outer_vertices, outer_edges

def find_closest_edge_by_angle(v0,v1,x,edges):
    def angle(v):
        u = x[v]-x[v1]
        return np.arctan2(u[1], u[0])
    origin = angle(v0)
    def vertex_from_edge(e):
        s = set(e)
        s.remove(v1)
        return s.pop()
    vertices = [vertex_from_edge(e) for e in edges if v1 in e and v0 not in e]
    def angle_from_origin(v):
        return (angle(v)-origin)%(2*np.pi)
    vertices = [v for v in vertices if angle_from_origin(v)>1e-3]

    return min(vertices, key=angle_from_origin) 

def find_intersections(v1, v2, x, edges):
    p1, p2 = x[v1], x[v2]
    x1, y1 = p1
    x2, y2 = p1

    def intersection_impossible(e):
        v3, v4 = e
        x3, y3 = x[v3]
        x4, y4 = x[v4]
        # return (max(x3,x4)<min(x1,x2)) or (max(y3,y4)<min(y1,y2)) or (min(x3,x4)>max(x1,x2)) or (min(y3,y4)>max(y1,y2))
        return False

    edges = [e for e in edges if not(intersection_impossible(e)) and not(v1 in e or v2 in e)]
    R = np.array([[0,1],[-1,0]])
    n12 = R@(p2-p1)
    u12 = (p2-p1)/np.linalg.norm(p2-p1)**2
    def find_intersection(e):
        v3, v4 = e
        p3, p4 = x[v3], x[v4]
        n34 =  R@(p4-p3)
        u34 = (p4-p3)/np.linalg.norm(p4-p3)**2
        A = np.array([n12, n34])
        b = np.array([np.dot(n12,p1), np.dot(n34,p3)])
        try:
            p = np.linalg.solve(A, b)
        except np.linalg.LinAlgError:
            return
        l12, l34 = np.dot(p-p1, u12), np.dot(p-p3, u34)
        if l12 > 0 and l12 <1. and l34 > 0 and l34 < 1:
            return p, e, l12
    intersections = [tup for tup in map(find_intersection, edges) if tup is not None]
    if intersections:
        return min(intersections, key=lambda t:t[2])[:2]

def remove_edge(edges, v1, v2):
    try:
        edges.remove([v1, v2])
    except ValueError:
        pass
    try:
        edges.remove([v2, v1])
    except ValueError:
        pass

def compute_outline_from_boundary(x_out, outeredges):
    cHull = ConvexHull(x_out)
    start = cHull.vertices[0]
    prev = cHull.vertices[-1]
    current = start
    shadow = [start]
    terminated = False
    for _ in range(len(x_out)):
        next_candidate = find_closest_edge_by_angle(prev, current, x_out, outeredges)
        t = find_intersections(current, next_candidate, x_out, outeredges)
        if t is not None:
            p, e = t
            v1, v2 = e
            x_out = np.concatenate([x_out, p.reshape(1,-1)])
            vp = x_out.shape[0]-1
            outeredges.append([current, vp])
            outeredges.append([next_candidate, vp])
            outeredges.append([v1, vp])
            outeredges.append([v2, vp])
            remove_edge(outeredges, current, next_candidate)
            remove_edge(outeredges, v1, v2)
            next_ = vp
        else:
            next_ = next_candidate
        if next_==start:
            terminated = True
            break
        shadow.append(next_)
        prev, current = current, next_

    if not(terminated):
        raise Exception('Too many iterations')
    
    return shadow, x_out

def compute_outline(body_positions, edges, tetras, return_type='adjacency'):
    x_out, outeredges = compute_boundary(body_positions, edges, tetras)
    shadow, x_out = compute_outline_from_boundary(x_out, outeredges)

    indices = np.unique(shadow)
    x_out = x_out[indices]
    remapped_shadow = np.empty_like(shadow)
    for i_new, i in enumerate(indices):
        remapped_shadow[shadow==i] = i_new
    if return_type == 'adjacency':
        return x_out, remapped_shadow
    elif return_type == 'vector':
        return x_out[remapped_shadow]


def plot_outline(ax, outline_nodes, outline_edges, **kwargs):
    for i in range(len(outline_edges)):
        v1, v2 = outline_edges[i-1], outline_edges[i]
        ax.plot([outline_nodes[v1,0], outline_nodes[v2,0]], [outline_nodes[v1,1], outline_nodes[v2,1]], **kwargs)

def main(data):
    x = data['body_positions'][-1]
    shadow = x[:,:2]
    edges = data['edges']
    tetra = data['tets']

    x_out, outeredges = compute_boundary(x, edges, tetra)
    shadow, x_out = compute_outline_from_boundary(x_out, outeredges)

    return x_out, outeredges, shadow

if __name__ == '__main__':
    data = np.load(default_cfg.recordDirectory/default_cfg.recordFilename)
    x_out, outeredges, shadow = main(data)

    plt.scatter(x_out[:,0], x_out[:,1])
    for e1, e2 in outeredges:
        plt.plot([x_out[e1,0], x_out[e2,0]], [x_out[e1,1], x_out[e2,1]], 'g')
    for i in range(len(shadow)):
        v1, v2 = shadow[i-1], shadow[i]
        plt.plot([x_out[v1,0], x_out[v2,0]], [x_out[v1,1], x_out[v2,1]], 'c', lw=3)


    plt.axis('equal')
    plt.show()