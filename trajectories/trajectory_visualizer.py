import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.patches import Polygon
import os
from trajectory_builder import *
import argparse

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


def load_muscles():
    segments = np.load(os.path.join(PROJECT_DIR, 'assets', 'muscle_outlines.npz'))
    segments = {int(k[2:]):v for k,v in segments.items()}
    # segments = [s[1] for s in segments]
    maxx, maxy = -np.inf, -np.inf
    miny = np.inf
    muscles = {}
    for id, points in segments.items():
        points = points[:,[1,0]]
        maxx = max(np.max(points[:,0]), maxx)
        maxy = max(np.max(points[:,1]), maxy)
        miny = min(np.min(points[:,1]), miny)
        muscles[id] = points

    reverse_muscles = {k+50:m for k,m in muscles.items()}
    for k, points in muscles.items():
        muscles[k] = np.array([[maxx, maxy]]) - points
        reverse_muscles[k+50] = 2*np.array([[0, maxy-miny+5]]) + np.array([[1, -1]])*muscles[k]
    muscles.update(reverse_muscles)
    return muscles

def animate_trajectory(trajectory, duration):
    muscles = load_muscles()
    segmented_muscles = {}
    for segment in range(3,10):
        for id, muscle in muscles.items():
            segmented_muscles[100*segment+id] = muscle + (segment-3)*np.array([[180,0]])



    fig = plt.figure()
    ax = plt.axes()
    ax.set_xlim(0,1200)
    ax.set_ylim(-10,1084)
    ax.axis('equal')
    ax.set_axis_off()
    cmap = mpl.colormaps['magma']
    
    muscle_polys = {}
    for id in segmented_muscles:
        muscle_poly = Polygon(segmented_muscles[id], closed=True, fc=cmap(0), ec='k')
        muscle_polys[id] = muscle_poly
        ax.add_patch(muscle_poly)

    def update_anim(i):
        t = i/64
        activations = trajectory(t)
        for id in muscle_polys:
            muscle_polys[id].set_facecolor(cmap(activations[f'muscle_{id}']))
        return muscle_polys.values()

    ani = FuncAnimation(fig, update_anim, frames=int(duration*64), interval=1000/64, blit=True)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('traj', type=str, help='roll, crawl or a filename')

    args = parser.parse_args()
    if args.traj in ['roll', 'crawl', 'cshape']:
        duration = 1.0

        match args.traj:
            case 'roll':
                traj = manualRollTrajectory.create_trajectory()
            case 'crawl':
                traj = manualCrawlTrajectory.create_trajectory()
            case 'cshape':
                traj = manualCShapeTrajectory.create_trajectory()

    else:
        raise NotImplementedError()
    animate_trajectory(traj, duration)