import numpy as np
import json
import collections

class ContinuousBinaryRepeatedTrajectory:
    def __init__(self, duration=1, segments=range(3,10), groups=None, muscles=list(range(1,31))+list(range(51,81)), activations=None, delay='segment'):
        self.duration = duration
        self.segments = segments
        self.muscles = muscles
        self.delay = self.duration/len(self.segments) if delay == 'segment' else delay
        self.groups = [[i] for i in muscles] if groups is None else groups
        self.activations = activations
        assert(len(self.groups) == len(self.activations))


    def create_trajectory(self):
        def traj(t):
            t = t%self.duration
            a = {}
            min_segment = min(self.segments)
            for segment in self.segments:
                for group, (t_on, t_off) in zip(self.groups, self.activations):
                    shifted_time = t-(segment-min_segment)*self.delay
                    current_activation = np.searchsorted(t_on, shifted_time)
                    for muscle in group:
                        if current_activation > 0 and t_on[current_activation-1] <= shifted_time and t_off[current_activation-1] > shifted_time:
                            a[f'muscle_{segment*100+muscle}'] = 1.
                        else:
                            a[f'muscle_{segment*100+muscle}'] = 0.
            return a
        return traj
    
    def save(self, filename):
        if not(filename.endswith('.cbrtraj')):
            raise ValueError('Invalid filename. Filename must have .cbrtraj extension.')
        state = dict(duration=self.duration,
                     n_segments = self.n_segments,
                     n_muscles=self.n_muscles,
                     delay=self.delay,
                     groups=self.groups,
                     activations=[(list(x), list(y)) for (x, y) in self.activations])
        with open(filename, 'w') as f:
            json.dump(state, f, indent=4)

    def load(self, filename):
        if not(filename.endswith('.cbrtraj')):
            raise ValueError('Invalid filename. Filename must have .cbrtraj extension.')
        with open(filename, 'r') as f:
            state = json.load(f)
        self.duration=state['duration'],
        self.n_segments = state['n_segments'],
        self.n_muscles=state['n_muscles'],
        self.delay=state['delay'],
        self.groups=state['groups'],
        self.activations=[(np.array(x), np.array(y)) for (x, y) in state['activations']]


n_muscles = 60
roll_groups = [[1,9], [10,2], [3,11,19,18], [20,4,21,22,23,24], [5,8], [12,13], [6,7,14,30], [25,26,27,28], [15,16,17,29]]
roll_groups = roll_groups + [[m+50 for m in group] for group in reversed(roll_groups)]
roll_groups = roll_groups[3:]+roll_groups[:3]
duration = 1 
n = len(roll_groups)
roll_activations = [(np.array([duration*max(i-1,0)/n]),np.array([duration*(i+1)/n])) for i in range(n)]
roll_activations[0][0]    
manualRollTrajectory = ContinuousBinaryRepeatedTrajectory(duration, groups=roll_groups, activations=roll_activations, delay=0.)

n_muscles = 60
cshape_groups = [[i for i in range(1,31)], [i for i in range(51,81)]]
duration = 1 
n = len(cshape_groups)
cshape_activations = [(np.array([0]),np.array([duration])), (np.array([]), np.array([]))]    
manualCShapeTrajectory = ContinuousBinaryRepeatedTrajectory(duration, groups=cshape_groups, activations=cshape_activations, delay=0.)

n_segments = 7
n_muscles = 60
duration=1.0
crawl_activations = [(np.array([0.]),np.array([duration/n_segments])) for _ in range(n_muscles)]
manualCrawlTrajectory = ContinuousBinaryRepeatedTrajectory(duration, activations=crawl_activations)


class ContinuousSmoothRepeatedTrajectory:
    def __init__(self, duration=1, segments=range(3,10), groups=None, muscles=list(range(1,31))+list(range(51,81)), activations=None, delay='segment'):
        self.duration = duration
        self.segments = segments
        self.muscles = muscles
        self.delay = self.duration/len(self.segments) if delay == 'segment' else delay
        self.groups = [[i] for i in muscles] if groups is None else groups
        self.activations = activations
        assert(len(self.groups) == len(self.activations))

    def create_trajectory(self):
        def traj(t):
            t = t%self.duration
            a = {}
            min_segment = min(self.segments)
            for segment in self.segments:
                for group, activation in zip(self.groups, self.activations):
                    shifted_time = t-(segment-min_segment)*self.delay
                    for muscle in group:
                        a[f'muscle_{segment*100+muscle}'] = activation(shifted_time)
                        
            return a
        return traj

n_segments = 7
n_muscles = 60
duration=1.0

def bump(start, on_ramp, sustain, off_ramp, end):
    t0 = start
    c0 = t0+on_ramp/2
    t1 = t0+on_ramp
    t2 = t1+sustain
    c2 = t2+off_ramp/2
    t3 = t2+off_ramp
    assert(t3<end)
    def bump(x):
        return np.where(np.logical_and( x>t0, x<c0), .5*(x-start)**2/(c0-start)**2,
               np.where(np.logical_and(x>=c0, x<t1), 1-.5*(x-t1)**2/(c0-t1)**2,
               np.where(np.logical_and(x>=t1, x<t2), 1.,
               np.where(np.logical_and(x>=t2, x<c2), 1-.5*(x-t2)**2/(c2-t2)**2,
               np.where(np.logical_and(x>=c2, x<t3), .5*(x-t3)**2/(c2-t3)**2, 
                                                     0)))))
    return bump
crawl_activations = [bump(0.,.1,.1,.1,1) for _ in range(n_muscles)]

manualCrawlTrajectory = ContinuousSmoothRepeatedTrajectory(duration, activations=crawl_activations)
